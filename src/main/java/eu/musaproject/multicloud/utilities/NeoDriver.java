package eu.musaproject.multicloud.utilities;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.neo4j.driver.v1.*;

/**
 * Hello world!
 *
 */
public class NeoDriver 
{
    public static void main( String[] args )    {
    
    	Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
    	Session session = driver.session();

    	session.run( "CREATE (a:Person {name:'Arthur', title:'King'})" );

    	StatementResult result = session.run( "MATCH (a:Person) WHERE a.name = 'Arthur' RETURN a.name AS name, a.title AS title" );
    	while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    System.out.println( record.get( "title" ).asString() + " " + record.get("name").asString() );
    	}

    	session.close();
    	driver.close();
    	}
    
    public static void execute (String statement) {
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
    	Session session = driver.session();
    	session.run( statement );
    	session.close();
    	driver.close();
    }

    private static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader (file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }
    
    public static void executeFromFile (String Filename) throws IOException {
    	String statement=readFile(Filename);
    	execute(statement);	
    }

    public static void cleanDB () throws IOException {
    	String statement="match (n) detach delete n";
    	execute(statement);	
    }

}

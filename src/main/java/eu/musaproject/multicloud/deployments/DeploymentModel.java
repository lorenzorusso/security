package eu.musaproject.multicloud.deployments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.musaproject.multicloud.models.macm.MACM;

public class DeploymentModel {
	public int[] logical; 
	public int[] concrete; 
	//	 final int nPartitions;

	public DeploymentModel( int[] v1,  int[] v2){ // , final int nP) {
		logical=v1.clone();
		concrete = v2.clone(); 
		//		 nPartitions=nP;
	} 

	public void print() {
		String lex="";
		for (int i=0;i<logical.length;i++)
			lex+=" "+logical[i];
		lex+=" - ";
		for (int j=0;j<concrete.length;j++)
			lex+=" "+concrete[j];
		System.out.println(lex);
	}

	public void print2() {
		int count=0;
		int vmindex;
		int i,j;
		Map<Integer,Integer> used=new HashMap<Integer,Integer>();
		for (i=0; i<logical.length;i++) {
			vmindex=logical[i];
			if (!used.containsKey(vmindex)) {	
				used.put(vmindex,concrete[vmindex]);
			}
		}
		String lex="";
		for (i=0;i<logical.length;i++)
			lex+=" "+logical[i];
		lex+=" - ";
		for (Map.Entry<Integer,Integer> entry: used.entrySet()) {
			lex+=" "+entry.getValue();
		}
		System.out.println(lex);				
	}
	
	public int[] convert() {
		int[] fordeploy=new int[logical.length];
		for (int i=0;i<logical.length;i++) {
			fordeploy[i]=concrete[logical[i]];
		}
		return fordeploy;
	}
	
	static public void main(String args[]) {
		int[] v1={4,4,4,1,4};
		int[] v2={1,0,2,2,0};
		DeploymentModel d=new DeploymentModel(v1,v2);
		d.print();
		d.print2();
	}
} 


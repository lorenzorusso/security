package eu.musaproject.multicloud.deployments;

public class Component {
	int ID;
	public String Name;
	public int RAM;
	public int CPU;
	
	public Component(String Params) {
		String[] cdata=Params.split(",");
		ID=Integer.parseInt(cdata[0]);
		Name=cdata[1];
		RAM=Integer.parseInt(cdata[2]);
		CPU=Integer.parseInt(cdata[3]);

	}
}

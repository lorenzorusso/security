package eu.musaproject.multicloud.deployments;

import java.io.IOException;
import java.util.Scanner;

import eu.musaproject.multicloud.deployments.optimization.GeneticOptimizer2;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.utilities.NeoDriver;

public abstract class Deployer {
	protected MACM mymacm;
	protected Deployment myDeployment;
	protected static int evaluated;
	protected static double finalcost;
	
	public abstract Deployment deploy();
	
	public Deployer(){
		myDeployment=null;
		mymacm=null;
		evaluated=0;
	}

	public Deployer(MACM m) {
		setMACM(m);
		evaluated=0;
	}
	
	public void Offerings(String offering) {
		myDeployment.readOfferings(offering);
	}
	public void setMACM(MACM m) {
		mymacm=m;
		myDeployment=new LexicalDeployment();
		myDeployment.setApp(mymacm);
//		myDeployment.readOfferings("src/main/resources/optimization/test1.properties");
//		Component[] comps=new Component[mymacm.getPeers().size()];
//		int i=0;
//		for (Entry<String, Peer> entry: mymacm.getPeers().entrySet()){
//			if (entry.getValue().getType()==macmPeerType.SaaS) {
//				comps[i]=new Component(""+i+","+entry.getValue().getName()+",1024,100");
//			}
//			i++;
//		}
//		myDeployment.setApp(comps);
	}

	public MACM getDeployable() {
		return myDeployment.getDeployable(mymacm);
	}
	
//	public MACM getDeployable() {
//		MACM m=mymacm;
//		HashMap<VM,Integer> VMs=new HashMap<VM,Integer>();
//		HashMap<String,Integer> CSPs=new HashMap<String,Integer>();
//		if (myDeployment.status!=DeploymentMapBased.DeploymentStates.deployed) {
//			System.out.println("Deployment not evaluated");
//		} else {
//			for (Map.Entry<Component, VM> entry : myDeployment.deployment.entrySet()) {
//				if (!VMs.containsKey(entry.getValue())) {
//					VMs.put(entry.getValue(),1);
//					m.addIaaService(entry.getValue().VMtype);
//					if (!CSPs.containsKey(entry.getValue().CSP)) {
//						CSPs.put(entry.getValue().CSP, 1);
//						m.addCSP(entry.getValue().CSP);
//					}
//					m.addProvides(entry.getValue().CSP, entry.getValue().VMtype);
//				}
//				m.addHosts(entry.getValue().VMtype, entry.getKey().Name);
//				
//			}
//		}
//		return m;
//	}
	public abstract void refreshDeployment();
	
	public void printDeployment() {
		myDeployment.print();
	}

	public void printDeploymentData() {
		myDeployment.printData();
		System.out.println("Comps: "+myDeployment.Cindex.length);
		System.out.println("Offs: "+myDeployment.offering.length);
	}

	public static void main(String args[]) {
		int i,N=1;//50;
		double[] costs=new double[N];
		int[] evaluations=new int[N];
		double cmean=0;
		int evmean=0;
		String c="",ev="";
		try {
			for(i=0;i<N;i++) {
				NeoDriver.cleanDB();
				NeoDriver.executeFromFile("src/main/resources/Models/LHSabstract");
				MACM macm= new MACM();
				macm.readNeo();
				//			Deployer deployer=new GeneratorOptimizer();
				Deployer deployer=new GeneticOptimizer2();
				deployer.setMACM(macm);
				Scanner scan= new Scanner (System.in);
				System.out.println("The abstract application is in Graph DB,  press enter to deploy2");
				scan.nextLine();
				deployer.deploy();
				deployer.refreshDeployment();
				MACM m2=deployer.getDeployable();
				NeoDriver.cleanDB();
				m2.writeNeo();
				deployer.printDeployment();
				costs[i]=deployer.finalcost;
				c+=" "+String.format( "%1$,.2f",costs[i]);
				cmean+=costs[i];
				evaluations[i]=deployer.evaluated;
				ev+=" "+evaluations[i];
				evmean+=evaluations[i];
			}
			System.out.println("Mean Cost:"+String.format( "%1$,.2f",cmean/N)+" Costs:"+c);
			System.out.println("Mean Evaluations:"+(evmean/N)+" Evaluations:"+ev);
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

package eu.musaproject.multicloud.deployments.generators;

import java.util.ArrayList;
import java.util.List;

public class PartitionGenerator {
	List<Integer> Partitions;
	
	public PartitionGenerator(int n){
		Partitions=new ArrayList<Integer>();
		for(int i=0;i<n;i++) {
			Partitions.add(i,1);
		}
	}
	
	public void print(int N) {
		int i;
		System.out.print("[");
		for (i=1;i<N+1;i++) {
			System.out.print(" "+Partitions.get(i));
		}
		System.out.println("]");
	}
	public void recpartition(int m, int B, int N)  {
//		System.out.println(" "+m+" "+B+" "+N);
		int i;
		if(m==0) {
			print(N);
		} else {
			for(i=1;i<Math.min(B, m)+1;i++){
				Partitions.add(N+1, i);
				recpartition(m-i,i,N+1);
			}
		}
	}
	
	public void partitions(int n) {
		recpartition(n, n, 0);
	}
	public static void main(String args[]){
		PartitionGenerator pg=new PartitionGenerator(3);
		pg.partitions(15);
	}
}

package eu.musaproject.multicloud.deployments.generators;

public class CombinationGenerator extends DistributionGenerator {
	int dimK,dimN;
	int counter;
	int[] lexical;

	public CombinationGenerator(int k,int n ) {
		//k dimension of vector, N different possible values
		//Example k: VMs and N: CSPs
		dimK=k;
		dimN=n;
	}
	
	@Override
	public int[] start() {
		// return first value
		counter=0;
		lexical=new int[dimK];
		for(int i=0;i<dimK;i++) lexical[i]=0;
		return lexical;
	}

	@Override
	public int[] next() {
		int val=++counter;
		int zeros=0;
		for(int i=0;i<dimK;i++) {
			lexical[i]=val%dimN;
			zeros+=lexical[i];
			val=(int) val/dimN;
		}
		if (zeros!=0) return lexical;
		else return null;
	}


	public void printLexical(int[] s) {
		// TODO Auto-generated method stub
//		for (int i=0;i<dimK;i++) {
		for (int i=0;i<s.length;i++) {
			System.out.print("vm_"+(i+1)+"->CSP_"+(s[i]+1)+" ");
		}
		System.out.println("");
	}
	
	@Override
	public void printLexical() {
		printLexical(lexical);
	}
	
}

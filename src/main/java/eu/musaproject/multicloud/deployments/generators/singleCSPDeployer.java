package eu.musaproject.multicloud.deployments.generators;

public class singleCSPDeployer extends SetPartitionsLexicalOrderGenerator {

	public singleCSPDeployer(int n) {
		super(n);
	}
	
	public int numberOfVMs() {
		return super.numberOfSets();
	}
	
	public void printLexical(int s[]) {
		System.out.print("n of VMs:"+numberOfVMs()+" ");
		for (int i=0;((i<s.length)&&(s[i]!=0));i++) {
			System.out.print("c["+i+"]->vm_"+s[i]+" ");
		}
		System.out.println("");
	}
}

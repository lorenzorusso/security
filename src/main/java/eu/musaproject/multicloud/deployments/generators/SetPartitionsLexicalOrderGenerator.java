package eu.musaproject.multicloud.deployments.generators;

public class SetPartitionsLexicalOrderGenerator extends DistributionGenerator {
	int[] lexical;
	int[] max;
	int dim;

	public SetPartitionsLexicalOrderGenerator(int n) {
		dim=n;
	}
	
	public int numberOfSets()  {
		int m=lexical[0];
		for (int i=0;i<lexical.length;i++) {
			m=Math.max(m, lexical[i]);
		}
		return m;
	}
	
	@Override
	public int[] start() {
		// TODO Auto-generated method stub
		lexical=new int[dim];
		max=new int[dim];
		
	    /* Put all the elements in the same subset. */
	    for (int i = 0; i < dim; ++i) {
	        lexical[i] = 1;
	        max[i] = 1;
	    }

		return lexical;
	}
	public boolean next(int s[], int m[], int n) {
	    /* Update s: 1 1 1 1 -> 2 1 1 1 -> 1 2 1 1 -> 2 2 1 1 -> 3 2 1 1 ->
	    1 1 2 1 ... */

	    int i = 0;
	    ++s[i];
	    while ((i < n - 1) && (s[i] > m[i] + 1)) {
	        s[i] = 1;
	        ++i;
	        ++s[i];
	    }
	 
	    /* If i is has reached n-1 th element, then the last unique partitiong
	    has been found*/
	    if (i == n - 1)
	        return false;
	 
	    /* Because all the first i elements are now 1, s[i] (i + 1 th element)
	    is the largest. So we update max by copying it to all the first i
	    positions in m.*/
	    int max = s[i];
	    for (i = i - 1; i >= 0; --i)
	        m[i] = max;
	 
	    return true;
	}
	
	@Override
	public int[] next() {
		if (next(lexical, max, dim)) 
			return lexical;
		else 
			return null;
	}

	public void printLexical(int s[]) {
		for (int i=0;((i<s.length)&&(s[i]!=0));i++) {
			System.out.print(""+s[i]+" ");
		}
		System.out.println("");
	}
	
	@Override
	public void printLexical() {
		printLexical(lexical);
	}
}

package eu.musaproject.multicloud.deployments.generators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SetPartitionGenerator {
	int dim;
	int lexical[];
	
	public int bell(int n) {
		//todo
		return 16;
	}
	public SetPartitionGenerator(int n) {
		dim=bell(n);
		lexical= new int[dim];
	}
	
	public void printLexical(int s[]) {
		for (int i=0;((i<s.length)&&(s[i]!=0));i++) {
			System.out.print("c["+i+"]->vm_"+s[i]+" ");
		}
		System.out.println("");
	}

	public void printp(int s[], int n) {
	    /* Get the total number of partitions. In the exemple above, 2.*/
	    int part_num = 1;
	    int i;
	    for (i = 0; i < n; ++i)
	        if (s[i] > part_num)
	            part_num = s[i];
	 
	    /* Print the p partitions. */
	    
	    int p,j=0;
	    for (p = part_num; p >= 1; --p) {
	        System.out.print("{");
	        /* If s[i] == p, then i + 1 is part of the pth partition. */
	        for (i = 0; i < n; ++i) {
	            if (s[i] == p) 
	            		j=i+1;
	        }
	    		System.out.print(""+j+", ");
	    }
        System.out.println("}");
	}

	public boolean next(int s[], int m[], int n) {
	    /* Update s: 1 1 1 1 -> 2 1 1 1 -> 1 2 1 1 -> 2 2 1 1 -> 3 2 1 1 ->
	    1 1 2 1 ... */

	    int i = 0;
	    ++s[i];
	    while ((i < n - 1) && (s[i] > m[i] + 1)) {
	        s[i] = 1;
	        ++i;
	        ++s[i];
	    }
	 
	    /* If i is has reached n-1 th element, then the last unique partitiong
	    has been found*/
	    if (i == n - 1)
	        return false;
	 
	    /* Because all the first i elements are now 1, s[i] (i + 1 th element)
	    is the largest. So we update max by copying it to all the first i
	    positions in m.*/
	    int max = s[i];
	    for (i = i - 1; i >= 0; --i)
	        m[i] = max;
	 
	    return true;
	}


	public void setpaprtitions(int n) {
		 /* s[i] is the number of the set in which the ith element
        should go */
		int s[]=new int[16];
		/* m[i] is the largest of the first i elements in s*/		 
	    int m[]=new int[16];; 
	    int i;
	    /* The first way to partition a set is to put all the elements in the same
	       subset. */
	    for (i = 0; i < n; ++i) {
	        s[i] = 1;
	        m[i] = 1;
	    }
	 
	    /* Print the first partitioning. */
//	    printp(s, n);
	    System.out.print("1: ");
	    printLexical(s);
	 
	    /* Print the other partitioning schemes. */
	    int k=1;
	    while (next(s, m, n)) {
	    		k++;	    	
		    System.out.print(k+": ");
	    		printLexical(s);
//	        printp(s, n);
	    }
	 
	}
	
	public static void main(String args[]) {
		SetPartitionGenerator spg=new SetPartitionGenerator(3);

		spg.setpaprtitions(9);
	}

}

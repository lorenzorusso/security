package eu.musaproject.multicloud.deployments.generators;

public abstract class DistributionGenerator {
	
	public abstract int[] start();
	public abstract int[] next();
	public abstract void printLexical();

	public static void main (String args[]) {
		int[] l;
		int k=0;
//		DistributionGenerator dg=new SetPartitionsLexicalOrderGenerator(3);
//		DistributionGenerator dg=new singleCSPDeployer(3);
//		DistributionGenerator dg=new CombinationGenerator(2,3);
		DistributionGenerator dg=new multiCSPDeployer(7,3);
		
		l=dg.start();
		while(l!=null) {
			k++;
		    System.out.println(k+": ");
			dg.printLexical();
			l=dg.next();
		}
	}
}

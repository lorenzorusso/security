package eu.musaproject.multicloud.deployments.generators;

public class multiCSPDeployer extends DistributionGenerator {
	int[] Logical;
	int[] Concrete;
	int nvms;
	int nCSPs;
	SetPartitionsLexicalOrderGenerator LogicalDeployment;
	CombinationGenerator ConcreteDeployment;
	
	
	public int getNvms() {
		return nvms;
	}

	public void setNvms(int nvms) {
		this.nvms = nvms;
	}

	public int getnCSPs() {
		return nCSPs;
	}

	public void setnCSPs(int nCSPs) {
		this.nCSPs = nCSPs;
	}

	public int[] getLogicalDeployment() {
		return Logical;
	}
	
	public int[] getConcreteDeployment() {
		return Concrete;
	}
	public multiCSPDeployer(int maxnvms, int maxnCSPs){
		nvms=maxnvms;
		nCSPs=maxnCSPs;
		LogicalDeployment=new singleCSPDeployer(maxnvms); // vary vms number
		ConcreteDeployment=null; //different .. vary map vm
	}
	
	@Override
	public int[] start() {
		// Generate the first Logical Deployment and distribute over CSPs 
		Logical=LogicalDeployment.start();
		ConcreteDeployment=new CombinationGenerator(LogicalDeployment.numberOfSets(),nCSPs);
		Concrete=ConcreteDeployment.start();
		return Logical;
	}

	@Override
	public int[] next() {
		Concrete=ConcreteDeployment.next();
		if(Concrete==null) {
			Logical=LogicalDeployment.next();
			if (Logical!=null) {
				ConcreteDeployment=new CombinationGenerator(LogicalDeployment.numberOfSets(),nCSPs);
				Concrete=ConcreteDeployment.start();
			}	
		}
		return Logical;
	}

	@Override
	public void printLexical() {
		LogicalDeployment.printLexical();
		ConcreteDeployment.printLexical();
	}
	public void printLexical(int[] logical, int[] concrete) {
		LogicalDeployment.printLexical(logical);
		ConcreteDeployment.printLexical(concrete);
	}
	
	 public void print() {
			String lex="";
			for (int i=0;i<Logical.length;i++)
				lex+=" "+Logical[i];
			lex+=" - ";
			for (int j=0;j<Concrete.length;j++)
				lex+=" "+Concrete[j];
			System.out.println(lex);
	 }
}

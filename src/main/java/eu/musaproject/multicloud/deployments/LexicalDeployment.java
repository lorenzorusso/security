package eu.musaproject.multicloud.deployments;

import java.util.HashMap;

import eu.musaproject.multicloud.models.macm.MACM;

public class LexicalDeployment extends Deployment {

	int[] logical;
	int[] concrete;
	
	@Override
	public void setApp(Component[] Comps) {
		logical=new int[Comps.length];
		Cindex=Comps;
	}

	@Override
	public MACM getDeployable(MACM m) {
		HashMap<Integer,VM> VMs=new HashMap<Integer,VM>();
		HashMap<String,Integer> CSPs=new HashMap<String,Integer>();
		VM tmpVM;
		String tmpName="";
		if (status!=DeploymentMapBased.DeploymentStates.deployed) {
			System.out.println("Deployment not evaluated");
		} else {
			for (int i=0;i<logical.length;i++) {
				tmpVM=offering[concrete[logical[i]]];
				if (!VMs.containsKey(logical[i])) {
					VMs.put(logical[i],tmpVM);
					tmpName="vm_"+tmpVM.VMtype+"_"+logical[i];
					m.addIaaService(tmpName);
					if (!CSPs.containsKey(tmpVM.CSP)) {
						CSPs.put(tmpVM.CSP, 1);
						m.addCSP(tmpVM.CSP);
					}
					m.addProvides(tmpVM.CSP, ""+tmpName);
				}
				m.addHosts(tmpName, Cindex[i].Name);				
			}
		}
		return m;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub

	}

	@Override
	public void printData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void printDeploymentData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void fromVector(int[] depl,int[]cdepl) {
		logical=depl.clone();
		concrete=cdepl.clone();
		Component tmpC;
		status=DeploymentStates.deployed;
	}

	public void fromModel(DeploymentModel dm) {
		
	}
	@Override
	public double cost() {
		
		return 0;
	}

	@Override
	public boolean valid() {
		boolean ok=true;
		int [] freecpu= new int[concrete.length];
		int [] freemem= new int[concrete.length];
		int vmindex=-1;
		int i;
		//setup the total avilable cpu and mem for each vm
		for (i=0;i<concrete.length;i++) {
			vmindex=concrete[i];
			freecpu[i]=offering[vmindex].CPU;
			freemem[i]=offering[vmindex].RAM;			
		}
		
		//allocate mem and cpu for each component
		for (i=0; i<Cindex.length;i++ ) {
			vmindex=logical[i]-1;
			freecpu[vmindex]-=Cindex[i].CPU;
			freemem[vmindex]-=Cindex[i].RAM;
			if ((freecpu[vmindex]<0)||(freemem[vmindex]<0)) ok=false;
		}
//		for (i=0;i<concrete.length;i++) {
//			System.out.println("VM_"+concrete[i]+" CPU: "+freecpu[i]+"RAM: "+freemem[i]);
//		}
//		if (ok) System.out.print("Valid ");
//		else System.out.print("Invalid "); 
		return ok;
	}

	
	

}

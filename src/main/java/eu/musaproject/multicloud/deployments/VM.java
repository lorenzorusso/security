package eu.musaproject.multicloud.deployments;

public class VM {
	int	ID;
	public String CSP;
	public String VMtype;
	public int RAM;
	public int CPU;
	public double cost;
	
	public VM(String props) {
		//TODO check for wrong string
		String[] vmdata=props.split(",");
		CSP=vmdata[0];
		VMtype=vmdata[1];
		cost=Double.parseDouble(vmdata[2]);
		RAM=Integer.parseInt(vmdata[3]);
		CPU=Integer.parseInt(vmdata[4]);
	}
	
	public String print() {
		String vm="("+CSP+","+VMtype+","+RAM+","+CPU+","+cost+")";
		return vm;
	}
}

package eu.musaproject.multicloud.deployments;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;


public class DeploymentMapBased extends Deployment{
//	Properties prop;
//	Component[] Cindex;
//	VM[] offering;
	Map <Component,VM> deployment;
	String OfferingFile="resources/optimization/offering.properties";
//	DeploymentStates status=DeploymentStates.uncreated;
//
//	enum DeploymentStates { uncreated, undeployed, deployed} 

	public DeploymentMapBased() {
		deployment=new HashMap<Component,VM>();
	}

	public boolean valid() {
		//TODO
		boolean ok=true;
		
		return ok;
	}
	
	public MACM getDeployable(MACM m) {
		HashMap<VM,Integer> VMs=new HashMap<VM,Integer>();
		HashMap<String,Integer> CSPs=new HashMap<String,Integer>();
		if (status!=DeploymentMapBased.DeploymentStates.deployed) {
			System.out.println("Deployment not evaluated");
		} else {
			for (Map.Entry<Component, VM> entry : deployment.entrySet()) {
				if (!VMs.containsKey(entry.getValue())) {
					VMs.put(entry.getValue(),1);
					m.addIaaService(entry.getValue().VMtype);
					if (!CSPs.containsKey(entry.getValue().CSP)) {
						CSPs.put(entry.getValue().CSP, 1);
						m.addCSP(entry.getValue().CSP);
					}
					m.addProvides(entry.getValue().CSP, entry.getValue().VMtype);
				}
				m.addHosts(entry.getValue().VMtype, entry.getKey().Name);
				
			}
		}
		return m;
	}
	

//	public void readComponent(String Filename) {
//		prop = new Properties();
//		try {
//			prop.load(new FileInputStream(Filename));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		String[] comps= (prop.getProperty("Comps")).split(";");
//		Cindex= new Component[comps.length];
//		for (int i=0;i<comps.length;i++) {
//			Cindex[i]= new Component(comps[i].replaceAll("\\s+",""));
//		}
//	}

	public DeploymentMapBased(Component[] Comps) {
		setApp(Comps);
	}
		

	
	public void setApp(Component[] Comps) {
		Cindex=new Component[Comps.length];
		for(int i=0;i<Comps.length;i++){
			Cindex[i]=Comps[i];
			deployment.put(Comps[i], null);
		}
		status=DeploymentStates.undeployed;
	}
	
	public int[] toVector() {
		int length=deployment.size();
		int[] depl =new int[length];
		return depl;
	}
	
	public void fromVector(int[] depl) {
		Component tmpC;
		VM tmpVM;
		deployment= new HashMap <Component,VM>() ;
		for (int i=0;i<depl.length;i++){
			tmpC=Cindex[i];
			tmpVM=offering[depl[i]];
			//System.out.println("put C["+i+"]("+tmpC.Name+") on VM"+tmpVM.VMtype);
			deployment.put(tmpC, tmpVM);
		}
		status=DeploymentStates.deployed;
	}
	
	public void print() {
		for (Map.Entry<Component, VM> entry:deployment.entrySet() ){
			System.out.println("Component "+entry.getKey().Name+" on "+entry.getValue().VMtype+" provided by "+entry.getValue().CSP);
		}
	}
	public void printData() {
		System.out.println(""+Cindex.length+" Components");
		for (int i=0;i<Cindex.length;i++){
			if (Cindex[i]==null) System.out.println("missing comp"); //TODO Generate excepption
			else System.out.println("Cindex["+i+"]="+Cindex[i].Name);
		}
	}

	@Override
	public void printDeploymentData() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void fromVector(int[] depl, int[] concrete) {
		// TODO Auto-generated method stub
		
	}

}

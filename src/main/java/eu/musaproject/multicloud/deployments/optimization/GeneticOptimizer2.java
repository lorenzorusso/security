package eu.musaproject.multicloud.deployments.optimization;

import static org.jenetics.engine.limit.bySteadyFitness;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.jenetics.*;
import org.jenetics.engine.*;
import org.jenetics.util.DoubleRange;
import org.jenetics.util.IntRange;
import org.jenetics.util.LongRange;

import eu.musaproject.multicloud.deployments.Component;
import eu.musaproject.multicloud.deployments.Deployer;
import eu.musaproject.multicloud.deployments.Deployment;
import eu.musaproject.multicloud.deployments.DeploymentMapBased;
import eu.musaproject.multicloud.deployments.DeploymentModel;
import eu.musaproject.multicloud.deployments.VM;
import eu.musaproject.multicloud.deployments.generators.multiCSPDeployer;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.macmPeerType;

public class GeneticOptimizer2 extends Deployer {

	private int maxVMS;
	private int Nc;	
	int nComponents;
	int nCSP;
	private static double costs[];
	private static VM[] offering;
	private static Component[] Cindex;
	Codec<DeploymentModel,IntegerGene> deplcodec;


	public EvolutionStatistics <Double, ?>	statistics=EvolutionStatistics.ofComparable();
	public Phenotype<IntegerGene , Double> bestp;
	public Genotype<IntegerGene> bestg;
	
	Predicate<? super Genotype<IntegerGene>> validator = gt-> {
		boolean res=valid(deplcodec.decode(gt));
		return res;
//		return deplcodec.decode(gt).valid();

	};
	
//	 final static class DeploymentModel { 
//		 final int[] logical; 
//		 final int[] concrete; 
////		 final int nPartitions;
//
//		 DeploymentModel(final int[] v1, final int[] v2){ // , final int nP) {
//			 logical=v1.clone();
//			 concrete = v2.clone(); 
////			 nPartitions=nP;
//		 } 
//		 
//		 void print() {
//				String lex="";
//				for (int i=0;i<logical.length;i++)
//					lex+=" "+logical[i];
//				lex+=" - ";
//				for (int j=0;j<concrete.length;j++)
//					lex+=" "+concrete[j];
//				System.out.println(lex);
//		 }
//		 
//		 int[] convert() {
//			 int[] fordeploy=new int[logical.length];
//			 for (int i=0;i<logical.length;i++) {
//				 fordeploy[i]=concrete[logical[i]];
//			 }
//			 return fordeploy;
//		 }
//	 } 

	static public boolean valid(DeploymentModel d) {
		boolean ok=true;
		int [] freecpu= new int[d.concrete.length];
		int [] freemem= new int[d.concrete.length];
		int vmindex=-1;
		int i;
		//setup the total avilable cpu and mem for each vm
		for (i=0;i<d.concrete.length;i++) {
			vmindex=d.concrete[i];
			freecpu[i]=offering[vmindex].CPU;
			freemem[i]=offering[vmindex].RAM;			
		}
		
		//allocate mem and cpu for each component
		for (i=0; i<Cindex.length;i++ ) {
			vmindex=d.logical[i];
			freecpu[vmindex]-=Cindex[i].CPU;
			freemem[vmindex]-=Cindex[i].RAM;
			if ((freecpu[vmindex]<0)||(freemem[vmindex]<0)) ok=false;
		}
//		for (i=0;i<d.concrete.length;i++) {
//			System.out.println("VM_"+i+" CPU: "+freecpu[i]+"RAM: "+freemem[i]);
//		}
//		if (ok) System.out.print("Valid ");
//		else System.out.print("Invalid "); 
		return ok;
	}
	 
	public GeneticOptimizer2() {
	}

	public GeneticOptimizer2(int VMs,int N) {
			maxVMS=VMs;
			Nc=N;
			costs=new double[VMs];
			for (int i=0;i<VMs;i++) {costs[i]=0.1*i;}
			statistics=null;
			bestp=null;
			bestg=null;
	}

	public GeneticOptimizer2(VM[] off, Component[] Comps) {
		offering=off;
		maxVMS=offering.length;
		Cindex=Comps;
		Nc=Cindex.length;
		statistics=null;
		bestp=null;
		bestg=null;
	}

	public GeneticOptimizer2(Deployment dep) {
		setDeployment(dep);
	}

	public void setCosts (double[] c) {
		costs=c;
	}
	
//	private static Double fitness_old(int[] dep){
//		double tot=0;
//		
//		for (int i=0; i<dep.length; i++){
//			tot+=costs[dep[i]];
//		}
//		return tot;
//	}
	
	private double cost(int[] concrete) {
		double c=0;
		int vmindex=-1;
		
		//get the concrete deployment
		for (int i=0; i<concrete.length;i++) {
			vmindex= concrete[i];
			c+=offering[vmindex].cost;
		}
		return c;
	}

	private static Double fitness(DeploymentModel d) {
		evaluated++;
		double cost=0;
		int vmindex=-1;
		Map<Integer,Integer> used=new HashMap<Integer,Integer>();
//		d.print2();
//		String lex="";
//		for (int i=0;i<d.logical.length;i++)
//			lex+=" "+d.logical[i];
//		lex+=" - ";
//		for (int j=0;j<d.concrete.length;j++)
//			lex+=" "+d.concrete[j];
//		System.out.println(lex);
		//get the concrete deployment
		for (int i=0; i<d.logical.length;i++) {
			vmindex=d.logical[i];
			if (!used.containsKey(vmindex)) {		
				cost+=offering[d.concrete[vmindex]].cost;
				used.put(vmindex, 1);
			}
		}
		if (!valid(d)) cost=cost*10000000;
		return cost;
	}
			
	public void optimize() {
		System.out.println(""+maxVMS+" "+offering.length);
		deplcodec = Codec.of(
				 codecs.ofVector(IntRange.of(0,nComponents-1 ), nComponents),
				 codecs.ofVector(IntRange.of(0, nCSP-1), nComponents),
				 DeploymentModel::new);
		Engine<IntegerGene , Double> engine = Engine
				.builder(
						GeneticOptimizer2::fitness,
//						codecs.ofVector(IntRange.of(0, maxVMS-1),Nc),
//						codecs.ofVector(IntRange.of(0, Nc),offering.length))
						deplcodec)
		.populationSize(500)
		.genotypeValidator(validator)
		.optimize(Optimize.MINIMUM)
		.selector(new BoltzmannSelector<>())
		.alterers (new SinglePointCrossover<>(0.2),new Mutator<>(0.1))
//				new MultiPointCrossover<>(0.2))
		.build( );
		
		//Create Evolution Statistics
		EvolutionStatistics <Double, ?>	stats=EvolutionStatistics.ofComparable() ;
		bestp = engine.stream()
				.limit(bySteadyFitness(100))
				.limit(1000)
				.peek(stats)				
				.collect(EvolutionResult.toBestPhenotype()) ;
		statistics=stats;
//		System.out.println(statistics);
		//System.out.println(bestp);
		DeploymentModel d=deplcodec.decode(bestp.getGenotype());
		d.print2();
		valid(d);
//		d.printOffering();
		finalcost=bestp.getFitness();
		System.out.println("Cost:"+finalcost+" Evaluated:"+evaluated);
	}

	public void setDeployment(Deployment dep) {
		myDeployment=dep;
		offering=myDeployment.getOffering();
		maxVMS=offering.length;
		Cindex=myDeployment.getComponents();
		Nc=Cindex.length;
		statistics=null;
		bestp=null;		
	}
	private void prepare() {
		List<Peer> components=mymacm.getPeersByType(macmPeerType.SaaS,macmPeerType.PaaS);
		nComponents=components.size();
		offering=myDeployment.getOffering();
		Cindex=myDeployment.getComponents();
		nCSP=offering.length;
		System.out.println("nComponents: "+nComponents+" nOfferings:"+nCSP);
	}
	
	@Override
	public DeploymentMapBased deploy() {
		prepare();
		optimize();
		int[] result=bestp.getGenotype().getChromosome().as(IntegerChromosome.class).toArray();
		DeploymentModel d=deplcodec.decode(bestp.getGenotype());
		d.print();
		//TODO
		//convert in the generator representation ..
		//difference concrete has variable length
		myDeployment.fromVector(d.logical,d.concrete);
//		for (int i=0;i<Nc;i++) {
//			System.out.println("C["+i+"] on "+result[i]);
//		}
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void refreshDeployment() {
		setDeployment(myDeployment);
	}


	public static void main ( String [ ] args ) {
		Deployment dep= new DeploymentMapBased();
		dep.readComponent("src/main/resources/optimization/test1.properties");
		dep.readOfferings("src/main/resources/optimization/test1.properties");
		GeneticOptimizer2 opt=new GeneticOptimizer2(dep);
		opt.deploy();
		System.out.println(opt.statistics);
		System.out.println(opt.bestp);
		dep.print();
	}

}

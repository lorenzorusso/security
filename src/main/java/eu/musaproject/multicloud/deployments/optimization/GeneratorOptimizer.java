package eu.musaproject.multicloud.deployments.optimization;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jenetics.IntegerChromosome;

import eu.musaproject.multicloud.deployments.Component;
import eu.musaproject.multicloud.deployments.Deployer;
import eu.musaproject.multicloud.deployments.Deployment;
import eu.musaproject.multicloud.deployments.DeploymentMapBased;
import eu.musaproject.multicloud.deployments.VM;
import eu.musaproject.multicloud.deployments.generators.multiCSPDeployer;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class GeneratorOptimizer extends Deployer {
	multiCSPDeployer mcgen;
	int nComponents;
	int nCSP;
	VM[] offering;
	Component[] Cindex;
	int [] logical;
	int [] concrete;
	double cost;
	int runs;


	public GeneratorOptimizer() {
		mcgen=null;
	}
	
	public int getRuns() {
		return runs;
	}
	
	public boolean valid() {
		return enoughResource();
	}
	
	public double fitness() {
		return cost();
	}
	public void optimize(){
		int[] l=mcgen.start();
		int k=0;
		double tmpcost;
//		Scanner scan=new Scanner(System.in );
		while(l!=null) {
			k++;
		    System.out.println(k+": ");
		    mcgen.print();
		    tmpcost=fitness();
			if (valid()&&(cost>tmpcost)) {
				cost=tmpcost;
				logical=mcgen.getLogicalDeployment().clone();
				for (int i=0;i<logical.length;i++) {
					logical[i]--;
				}
				concrete=mcgen.getConcreteDeployment().clone();					
			}
			System.out.println("Previous Cost"+cost+ " Actual Cost: "+tmpcost);

//			System.out.print("Press any key to continue . . . ");
//			scan.nextLine();
			l=mcgen.next();
		}
		runs=k;
	}

	
	@Override
	public Deployment deploy() {
		prepare();
		optimize();
		System.out.println("Compared "+runs+" deployments");
//		mcgen.print();
		mcgen.printLexical(logical,concrete);
		System.out.println("Cost:"+cost);
		for (int i=0;i<logical.length;i++) {
			System.out.print(" "+logical[i]);
		}
		System.out.print(" - ");
		for (int i=0;i<concrete.length;i++) {
			System.out.print(" "+concrete[i]);
		}
		System.out.println("cost: "+cost);

		myDeployment.fromVector(logical,concrete);
		return myDeployment;
	}

	private void prepare() {
		List<Peer> components=mymacm.getPeersByType(macmPeerType.SaaS,macmPeerType.PaaS);
		nComponents=components.size();
		offering=myDeployment.getOffering();
		Cindex=myDeployment.getComponents();
		nCSP=offering.length;
		mcgen=new multiCSPDeployer(nComponents,nCSP);	
		System.out.println("nComponents: "+nComponents+" nOfferings:"+nCSP);
		cost=1000000000;
		logical=null;
		concrete=null;
	}


	protected double cost() {
		double c=0;
		int vmindex=-1;
		
		//get the concrete deployment
		int [] concrete=mcgen.getConcreteDeployment();
		for (int i=0; i<concrete.length;i++) {
			vmindex= concrete[i];
			c+=offering[vmindex].cost;
		}
		return c;
	}
	
	public boolean enoughResource() {
		boolean ok=true;
		int [] logical=mcgen.getLogicalDeployment();
		int [] concrete=mcgen.getConcreteDeployment();
		int [] freecpu= new int[concrete.length];
		int [] freemem= new int[concrete.length];
		int vmindex=-1;
		int i;
		//setup the total avilable cpu and mem for each vm
		for (i=0;i<concrete.length;i++) {
			vmindex=concrete[i];
			freecpu[i]=offering[vmindex].CPU;
			freemem[i]=offering[vmindex].RAM;			
		}
		
		//allocate mem and cpu for each component
		for (i=0; i<Cindex.length;i++ ) {
			vmindex=logical[i]-1;
			freecpu[vmindex]-=Cindex[i].CPU;
			freemem[vmindex]-=Cindex[i].RAM;
			if ((freecpu[vmindex]<0)||(freemem[vmindex]<0)) ok=false;
		}
//		for (i=0;i<concrete.length;i++) {
//			System.out.println("VM_"+concrete[i]+" CPU: "+freecpu[i]+"RAM: "+freemem[i]);
//		}
		if (ok) System.out.print("Valid ");
		else System.out.print("Invalid "); 
		return ok;
	}
	
	
	private DeploymentMapBased produceDeployment() {
		// TODO Auto-generated method stub
		myDeployment.fromVector(logical,concrete);
		return null;
	}

	@Override
	public void refreshDeployment() {
		// TODO Auto-generated method stub
		//create mcgen according to MACM...
		//create components map and ...
		myDeployment.fromVector(logical,concrete);
	}

	public static void main(String args[]) {
		try {
			NeoDriver.cleanDB();
			NeoDriver.executeFromFile("src/main/resources/Models/LHSabstract");
			MACM macm= new MACM();
			macm.readNeo();
			Deployer deployer=new GeneratorOptimizer();
			deployer.setMACM(macm);
			Scanner scan=new Scanner(System.in);
			System.out.println("Ready to optimize");
			System.out.print("Press any key to continue . . . ");
			scan.nextLine();
			deployer.deploy();	
			deployer.printDeployment();
			MACM m2=deployer.getDeployable();
			NeoDriver.cleanDB();
			m2.writeNeo();
			deployer.printDeployment();
//			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

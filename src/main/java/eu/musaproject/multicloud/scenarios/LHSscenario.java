package eu.musaproject.multicloud.scenarios;

import java.io.IOException;

import eu.musaproject.multicloud.converters.NIST_db2Neo;
import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.models.SLA.SLAgraphs;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class LHSscenario {

	public static void main(String args[]) {

		try {
			NeoDriver.cleanDB();
			NIST_db2Neo.createNISTgraph();
			NeoDriver.executeFromFile("src/main/resources/Models/LHS");

			csv2neo.createGraphFromCsv("src/main/resources/SLAs/SLA_OAUTH_Google","SLA");
			SLAgraphs.addSLA("IDMGoogle", "SLA_OAUTH_Google");

			csv2neo.createGraphFromCsv("src/main/resources/SLAs/SLA_VM_Ubuntu","SLA");
			SLAgraphs.addSLA("myvm", "SLA_VM_Ubuntu");

			csv2neo.createGraphFromCsv("src/main/resources/SLAs/SLA_TUT_JP","SLA");
			SLAgraphs.addComponentSLA("JP", "SLA_TUT_JP");

			SLAgraphs.missingSLA();
			SLAgraphs.SLAs();
			
//			MulticloudGraphs.CSPs();
			
			SLAgraphs.composeSLA("SLA_TUT_JP", "SLA_VM_Ubuntu", "composedSLA");
			
			SLAgraphs.SLAs();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

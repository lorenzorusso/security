package eu.musaproject.multicloud.scenarios;

import java.io.IOException;
import java.sql.SQLException;

import eu.musaproject.multicloud.converters.NIST_db2Neo;
import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.models.SLA.SLAgraphs;
import eu.musaproject.multicloud.models.engines.SLAreasoner;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class CS3M {

	public static void main(String args[]) throws IOException{
		NeoDriver.cleanDB();
		MACM mcapp=new MACM();
//		NIST_db2Neo.createNISTgraph();
		NeoDriver.executeFromFile("src/main/resources/CS3M/webapp.cyber");
		mcapp.readNeo();

		//Load CSP SLA 
		csv2neo.createGraphFromCsv("src/main/resources/CS3M/csp_sla","SLA");
		mcapp.addSLA("csp_sla");
		Relationship r=mcapp.addGrants("csp", "csp_sla");
		mcapp.syncNeoRelationship(r);
		macmSLA cspsla=new macmSLA();
		cspsla.readNeo("csp_sla");

		//Load VM SLA
		csv2neo.createGraphFromCsv("src/main/resources/CS3M/vm_slat","SLAT");
		mcapp.addSLAT("vm_slat");
		r=mcapp.addSupports("vm", "vm_slat");
		mcapp.syncNeoRelationship(r);
		macmSLA vmslat=new macmSLA();
		vmslat.readNeo("vm_slat");

		//Load DB SLA
		csv2neo.createGraphFromCsv("src/main/resources/CS3M/db_slat","SLAT");
		mcapp.addSLAT("db_slat");
		r=mcapp.addSupports("db", "db_slat");
		mcapp.syncNeoRelationship(r);
		macmSLA dbslat=new macmSLA();
		dbslat.readNeo("db_slat");

		//Load W SLA
		csv2neo.createGraphFromCsv("src/main/resources/CS3M/webapp_slat","SLAT");
		mcapp.addSLAT("webapp_slat");
		r=mcapp.addSupports("webapp", "webapp_slat");
		mcapp.syncNeoRelationship(r);
		macmSLA webappslat=new macmSLA();
		webappslat.readNeo("webapp_slat");
		
		//Create KNowledge base
		SLAreasoner slar;
		try {
			slar = new SLAreasoner(mcapp);
			slar.addSLAtoKB(cspsla);
			slar.addSLATtoKB(vmslat);	
			slar.addSLATtoKB(dbslat);	
			slar.addSLATtoKB(webappslat);	
			slar.addSLATtoKB(vmslat);	
			slar.evaluateCompositionRules("AC");

			macmSLA vmsla=slar.getSLA("vm_sla");
			vmsla.writeNeo();
			mcapp.addSLA(vmsla.getName());
			r=mcapp.addGrants("vm", "vm_sla");
			mcapp.syncNeoRelationship(r);

			macmSLA dbsla=slar.getSLA("db_sla");
			dbsla.writeNeo();
			mcapp.addSLA(dbsla.getName());
			r=mcapp.addGrants("db", "db_sla");
			mcapp.syncNeoRelationship(r);
			
			macmSLA webappsla=slar.getSLA("webapp_sla");
			webappsla.writeNeo();
			mcapp.addSLA(webappsla.getName());		
			r=mcapp.addGrants("webapp", "webapp_sla");
			mcapp.syncNeoRelationship(r);

			slar.query("slat(X,vm).");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//Compose
	}
}

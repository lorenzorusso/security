package eu.musaproject.multicloud.converters;

import java.io.StringReader;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import eu.musaproject.multicloud.models.engines.NISTrules;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.specs.datamodel.agreement.Context;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Terms;
import eu.specs.datamodel.agreement.terms.Terms.All;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.ControlFramework;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.Capabilities;
import eu.specs.datamodel.agreement.terms.Term;

public class macm2wsag {
	static NISTrules nist;
	
	public macm2wsag() throws SQLException {
		nist=new NISTrules();
		nist.UpdateControlsList();
	}
	
	public static AgreementOffer macm2wsagOffer(MACM macm,String provider) {
		nist=new NISTrules();
		try {
			nist.UpdateControlsList();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		AgreementOffer offer = new  AgreementOffer();
		macmSLA sla=new macmSLA();
		Map<String,NISTSecurityControl> controlsmap=nist.getControlsMap();
		sla.readNeo(provider+"_sla");
		//Set Agreement Name
		offer.setName(provider+"_sla");
		
		//Set Agreement Context
		Context c=new Context();
		c.setAgreementInitiator("user");
		c.setAgreementResponder(provider);
		c.setServiceProvider(provider);
		c.setTemplateName(provider+"_slat");
		offer.setContext(c);
		
		
		//Create Capability
		ControlFramework nistfmw=new ControlFramework();
		nistfmw.setFrameworkName("NIST Control framework 800-53 rev. 4");
		nistfmw.setId("NIST_800_53_r4");
		List<AbstractSecurityControl> scs=new ArrayList<AbstractSecurityControl>();

		List<Peer> controls=sla.getPeersByType(SLAPeerType.SecurityControl);
		for(int i=0;i<controls.size();i++) {
			//Get Control from MACM
			String scid=controls.get(i).getName();
			System.out.println("macm2wsag Adding "+ scid);
			//Create NIST xml representation
//			String [] scdata=scid.split("_");			
//			NISTSecurityControl sc=new NISTSecurityControl();
//			sc.setControlFamily(scdata[0].toUpperCase());
//			if(scdata.length>2) {
//				sc.setControlEnhancement(scdata[2]);				
//			}	
//			sc.setId(scid);
			//Add to capability
			NISTSecurityControl sc=controlsmap.get(scid.toLowerCase());
			scs.add(sc);
		}
		nistfmw.setSecurityControl(scs);

		//Set Agreement Terms
		ServiceDescriptionTerm sdt=new ServiceDescriptionTerm();
		sdt.setName(provider);
		sdt.setServiceName(provider);
		ServiceDescriptionType sd=new ServiceDescriptionType();
		Capabilities cs=new Capabilities();
		sd.setCapabilities(cs);
		

		//Add Capability
		CapabilityType capability =new CapabilityType();
		capability.setControlFramework(nistfmw);
		sd.getCapabilities().getCapability().add(capability);

		//NIST Terms pieces
		sdt.setServiceDescription(sd);
		List<Term> al=new ArrayList<Term>();
		al.add(sdt);
		Terms t=new Terms();
		All a=new All();	
		a.setAll(al);
		t.setAll(a);

		offer.setTerms(t);
		return offer;
	}
	
	public static String macm2wsagString(MACM macm,String provider) {
		AgreementOffer offer = macm2wsagOffer(macm,provider);		
		String wsag=null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		    StringWriter sw = new StringWriter();
			marshaller.marshal(offer,sw);
		    wsag = sw.toString();
		} catch (JAXBException e) {
			System.out.println("JAXBException "+e.toString());
		}
		return wsag;
	}
	
	public static void main(String args[]) {
		MACM macm=new MACM();
		macm.readNeo();
		macm.print();
		String a=macm2wsagString(macm,"db");
		System.out.println(a);
	}
}

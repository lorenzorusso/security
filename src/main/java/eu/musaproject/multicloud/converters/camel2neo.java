package eu.musaproject.multicloud.converters;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;

import eu.musaproject.multicloud.models.macm.MACM;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.ServiceProperties.VariableSet;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.agreement.terms.Terms.All;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.ControlFramework;
import eu.specs.datamodel.sla.sdt.ObjectiveList;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.Capabilities;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@SuppressWarnings("restriction")
public class camel2neo {


	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static MACM camel2graph(String camelFile) {

		MACM mcapp=new MACM();
		String camelString=null;
		Map <String,String > comms= new HashMap <String,String>();

		try {
			camelString=readFile(camelFile,StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StringTokenizer camelTokens = new StringTokenizer(camelString);
		int state=0;
		int count=0;
	
		while (camelTokens.hasMoreTokens()) {
			count++;
			//search for components
			String token=camelTokens.nextToken();
			if (state==1) System.out.println("State:"+state+" "+token);
			if (state==2) {
				System.out.println("count:"+count+"State:"+state+" "+token);
				mcapp.addSaaService(token);
				int compstate=0;
				boolean inside=true;
				token=camelTokens.nextToken();
				while (inside) {
					//setup communications
				}
				state=0;
			}else if (state==1) {
				if (token.equalsIgnoreCase("component")) {
					state=2;
				} else
					state=0;
			} else if (state==0) {
				if (token.equalsIgnoreCase("internal")) {
					System.out.println("GOT IT");
					state=1;
				}
			} 
		}
		return mcapp;
		
	}

	public static void camel2neo(String camelFile) {
		MACM mcapp = camel2graph(camelFile);
		mcapp.writeNeo();
	}
	public static void main(String args[]) {
		camel2neo.camel2neo("src/main/resources/Models/tut.camel");
	}
}

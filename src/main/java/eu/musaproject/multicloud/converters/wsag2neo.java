package eu.musaproject.multicloud.converters;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.ServiceProperties.VariableSet;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.agreement.terms.Terms.All;
import eu.specs.datamodel.control_frameworks.AbstractSecurityControl;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.ControlFramework;
import eu.specs.datamodel.sla.sdt.ObjectiveList;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.Capabilities;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@SuppressWarnings("restriction")
public class wsag2neo {

	public static AgreementOffer buildOfferFromXml(String xml) {
		AgreementOffer offer = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			offer = (AgreementOffer) unmarshaller.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			System.out.println("JAXBException "+e.toString());
		}
		return offer;
	}

	public String buildXmlFromOffer(AgreementOffer offer) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(offer, sw);
		return sw.toString();
	}

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static void wsagReader(String wsagFile) {
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();

		String xmlString=null;

		try {
			xmlString=readFile(wsagFile,StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int i,j,k;

		//Context INformation
		AgreementOffer offer=new AgreementOffer();
		offer=buildOfferFromXml(xmlString);
		String SLAname=offer.getName();
		String statement ="CREATE (sla:SLA {name:'"+SLAname+"'}) ";
		HashMap<String,Boolean> families =new HashMap<String,Boolean>();
		HashMap<String,Boolean> controlsmap =new HashMap<String,Boolean>();

		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();

		//Build Up Service Description Terms
		for (i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();

				List<CapabilityType> capabilityList = capabilities.getCapability();

				for (j=0; j<capabilityList.size();j++) {
					ControlFramework controlFramework = capabilityList.get(j).getControlFramework();
					List <AbstractSecurityControl> controls=controlFramework.getSecurityControl();

					for (k=0; k<controls.size();k++) {
						String controlName=null;
						NISTSecurityControl control=(NISTSecurityControl) controls.get(k);
						//If this is the first control of this family, add the family to the SLA

						if (control.getControlEnhancement().equals("0")) {
							controlName=control.getControlFamily()+"_"+control.getSecurityControl();
						} else {
							controlName=control.getControlFamily()+"_"+control.getSecurityControl()+"_"+control.getControlEnhancement();
						}
						if(families.get(control.getControlFamily())==null) {
							families.put(control.getControlFamily(), true);
							statement=statement+",";
							statement=statement+"("+control.getControlFamily()+":ControlFamily {name:'"+control.getControlFamily()+"'}),";
							statement=statement+"("+control.getControlFamily()+") -[:ControlFamilyIn]->(sla) ";
						}
						if(controlsmap.get(controlName)==null) {
							controlsmap.put(controlName, true);
							statement=statement+",";
							statement+="("+controlName+":SecurityControl {name:'"+controlName+"'}), ("+controlName+")-[:SecurityControlOf]->("+control.getControlFamily()+")" ;							
						}
					}
				}
			} 
		}

		//Add Metrics and Mapping against controls 
		List <Variable> variables=new ArrayList <Variable>();
		HashMap<String,HashSet<String>> metricsControlMap = new HashMap<String,HashSet<String>>();
		for (i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof ServiceProperties) {
				VariableSet vs=((ServiceProperties) terms.get(i)).getVariableSet();
				variables.addAll(vs.getVariables());
			}
		}
		for (i = variables.size() - 1; i >= 0; i--) {
			String location=variables.get(i).getLocation();
			String locs[]=location.split("//");
			statement=statement+",";
			statement=statement+"("+variables.get(i).getName()+":SecurityMetric {name:'"+variables.get(i).getName()+"'})";
			for (j=1; j<locs.length;j++) {
				String control=locs[j].substring(locs[j].indexOf("id='")+4,locs[j].indexOf("']"));
				control=control.substring(control.indexOf("NIST_")+5);
				statement=statement+",";
				statement=statement+"("+variables.get(i).getName()+") -[:MetricMappedTo]->("+control+") ";
			}
		}

		//Create SLOs and map them against metrics
		for (i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm guarantee = (GuaranteeTerm) terms.get(i);
				ObjectiveList objectiveList = guarantee
						.getServiceLevelObjective().getCustomServiceLevel()
						.getObjectiveList();
				List<SLOType> slos = objectiveList.getSLO();
				for (j=0;j < slos.size();j++) {
					String MetricREF=slos.get(j).getMetricREF();
					String operator="";
					String operand="";
					if (slos.get(j).getSLOexpression().getOneOpExpression()!=null) {
						operator= slos.get(j).getSLOexpression().getOneOpExpression().getOperator().toString();
						operand= slos.get(j).getSLOexpression().getOneOpExpression().getOperand().toString();
					}
					
					statement=statement+",";
					statement=statement+"("+slos.get(j).getSLOID()+":SLO {name:'"+slos.get(j).getSLOID()+"', operator:'"+operator+"', value:'"+operand+"'}),";
					statement=statement+"("+slos.get(j).getSLOID()+") -[:MeasuredWith]->("+MetricREF+"), ";
					statement=statement+"("+slos.get(j).getSLOID()+") -[:SLOin]->(sla) ";

				}
			}	
		}


//		//Create SLOs
//		for (i = terms.size() - 1; i >= 0; i--) {
//			if (terms.get(i) instanceof GuaranteeTerm) {
//				GuaranteeTerm guarantee = (GuaranteeTerm) terms.get(i);
//				ObjectiveList objectiveList = guarantee
//						.getServiceLevelObjective().getCustomServiceLevel()
//						.getObjectiveList();
//				List<SLOType> slos = objectiveList.getSLO();
//				for (j=0;j < slos.size();j++) {
//					String MetricREF=slos.get(j).getMetricREF();
//					String operator="";
//					String operand="";
//					if (slos.get(j).getSLOexpression().getOneOpExpression()!=null) {
//						operator= slos.get(j).getSLOexpression().getOneOpExpression().getOperator().toString();
//						operand= slos.get(j).getSLOexpression().getOneOpExpression().getOperand().toString();
//					}
//
//					HashSet<String> cset=metricsControlMap.get(MetricREF);
//					Iterator<String> it=cset.iterator();
//
//					statement=statement+",";
//					statement=statement+"("+MetricREF+":SecurityMetric {name:'"+MetricREF+"'})";
//
//					while (it.hasNext()) {
//						String ctrl=it.next();
//						statement=statement+",";
//						statement=statement+"("+MetricREF+") -[:MetricMappedTo]->("+ctrl+") ";
//						System.out.println(MetricREF+" "+operator+" "+operand+" "+ctrl);						
//					}					
//				}
//			}	
//		}

		System.out.println(statement);

		session.run(statement);

		session.close();
		driver.close();

	}

	public static void wsagGraph(String wsag, String type) {
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();

		String xmlString=wsag;

		int i,j,k;

		//Context INformation
		AgreementOffer offer=new AgreementOffer();
		offer=buildOfferFromXml(xmlString);
		String SLAname=offer.getName();
		String statement ="CREATE (sla"+":"+type+" {name:'"+SLAname+"'}) ";
		HashMap<String,Boolean> families =new HashMap<String,Boolean>();
		HashMap<String,Boolean> controlsmap =new HashMap<String,Boolean>();

		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();

		//Build Up Service Description Terms
		for (i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();

				List<CapabilityType> capabilityList = capabilities.getCapability();

				for (j=0; j<capabilityList.size();j++) {
					ControlFramework controlFramework = capabilityList.get(j).getControlFramework();
					List <AbstractSecurityControl> controls=controlFramework.getSecurityControl();

					for (k=0; k<controls.size();k++) {
						String controlName=null;
						NISTSecurityControl control=(NISTSecurityControl) controls.get(k);
						//If this is the first control of this family, add the family to the SLA

						if (control.getControlEnhancement().equals("0")) {
							controlName=control.getControlFamily()+"_"+control.getSecurityControl();
						} else {
							controlName=control.getControlFamily()+"_"+control.getSecurityControl()+"_"+control.getControlEnhancement();
						}
						if(families.get(control.getControlFamily())==null) {
							families.put(control.getControlFamily(), true);
							statement=statement+",";
							statement=statement+"("+control.getControlFamily()+":ControlFamily {name:'"+control.getControlFamily()+"'}),";
							statement=statement+"("+control.getControlFamily()+") -[:ControlFamilyIn]->(sla) ";
						}
						if(controlsmap.get(controlName)==null) {
							controlsmap.put(controlName, true);
							statement=statement+",";
							statement+="("+controlName+":SecurityControl {name:'"+controlName+"'}), ("+controlName+")-[:SecurityControlOf]->("+control.getControlFamily()+")" ;							
						}
					}
				}
			} 
		}

		//Add Metrics and Mapping against controls 
		List <Variable> variables=new ArrayList <Variable>();
		HashMap<String,HashSet<String>> metricsControlMap = new HashMap<String,HashSet<String>>();
		for (i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof ServiceProperties) {
				VariableSet vs=((ServiceProperties) terms.get(i)).getVariableSet();
				variables.addAll(vs.getVariables());
			}
		}
		for (i = variables.size() - 1; i >= 0; i--) {
			String location=variables.get(i).getLocation();
			String locs[]=location.split("//");
			statement=statement+",";
			statement=statement+"("+variables.get(i).getName()+":SecurityMetric {name:'"+variables.get(i).getName()+"'})";
			for (j=1; j<locs.length;j++) {
				String control=locs[j].substring(locs[j].indexOf("id='")+4,locs[j].indexOf("']"));
				control=control.substring(control.indexOf("NIST_")+5);
				statement=statement+",";
				statement=statement+"("+variables.get(i).getName()+") -[:MetricMappedTo]->("+control+") ";
			}
		}

		//Create SLOs and map them against metrics
		for (i = terms.size() - 1; i >= 0; i--) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm guarantee = (GuaranteeTerm) terms.get(i);
				ObjectiveList objectiveList = guarantee
						.getServiceLevelObjective().getCustomServiceLevel()
						.getObjectiveList();
				List<SLOType> slos = objectiveList.getSLO();
				for (j=0;j < slos.size();j++) {
					String MetricREF=slos.get(j).getMetricREF();
					String operator="";
					String operand="";
					if (slos.get(j).getSLOexpression().getOneOpExpression()!=null) {
						operator= slos.get(j).getSLOexpression().getOneOpExpression().getOperator().toString();
						operand= slos.get(j).getSLOexpression().getOneOpExpression().getOperand().toString();
					}
					
					statement=statement+",";
					statement=statement+"("+slos.get(j).getSLOID()+":SLO {name:'"+slos.get(j).getSLOID()+"', operator:'"+operator+"', value:'"+operand+"'}),";
					statement=statement+"("+slos.get(j).getSLOID()+") -[:MeasuredWith]->("+MetricREF+"), ";
					statement=statement+"("+slos.get(j).getSLOID()+") -[:SLOin]->(sla) ";

				}
			}	
		}

		System.out.println(statement);

		session.run(statement);

		session.close();
		driver.close();

	}

	public static void main(String args[]) {
		wsag2neo.wsagReader("src/main/resources/SLAs/wsag.xml");
	}
}

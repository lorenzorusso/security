package eu.musaproject.multicloud.converters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

public class NIST_db2Neo {

	 public static void main(String [] Arguments) {
		 NIST_db2Neo db=new NIST_db2Neo();
		 db.createNISTgraph();
	 }
	 
	 public static void createNISTgraph() {
		String NeoPayLoad;
		String statement="";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/slagenerator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "root");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Statement stmt = null;
			try {
				stmt = conn.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//Start generating the model
			statement=statement+"CREATE ";
			statement=statement+"(NIST:SLA {name:'NISTFramework', type:'SLAtree'}),";

			
			try {
				//Generate Security domains
				ResultSet rs = null;
				try {
					rs = stmt.executeQuery("select DISTINCT family_Id,family_name from controls ");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					try {
						while (rs.next()) {
							statement=statement+"("+rs.getObject(1)+":ControlFamily {name:'"+rs.getObject(1)+"', CFid:'"+rs.getObject(2)+"'}),";
							statement=statement+"("+rs.getObject(1)+") -[:ControlFamilyIn]->(NIST) ,";
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} finally {
					try {
						rs.close();
					} catch (Throwable ignore) {
						/*
						 * Propagate the original exception instead of this one
						 * that you may want just logged
						 */ }
				}
				
				//Generate Security Controls
				try {
					rs = stmt.executeQuery("select control_id,family_id from controls ");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					try {
						boolean start=true;
						while (rs.next()) {
							String SC=(String) rs.getObject(1);
							String CF=(String) rs.getObject(2);
							SC=SC.replace('-', '_');
							SC=SC.replace('(', '_');
							SC=SC.replace(')', ' ');
							if (start) {
								start=false;
							} else statement=statement+",";
							statement=statement+"("+SC+":SecurityControl {name:'"+SC+"'}),";
							statement=statement+"("+SC+") -[:SecurityControlOf]->("+CF+")";
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} finally {
					try {
						rs.close();
					} catch (Throwable ignore) {
						/*
						 * Propagate the original exception instead of this one
						 * that you may want just logged
						 */ }
				}
				System.out.println(statement);
				
				NeoPayLoad="{\"statements\": [{\"statement\":"+statement+"\"";
				NeoPayLoad=NeoPayLoad+" \"parameters\": null, \"resultDataContents\": [ \"row\", \"graph\" ], \"includeStats\": true";
				NeoPayLoad=NeoPayLoad+"}]}";
				
//				System.out.println(NeoPayLoad);
				
			} finally {
				try {
					stmt.close();
				} catch (Throwable ignore) {
					/*
					 * Propagate the original exception instead of this one that
					 * you may want just logged
					 */ }
			}
		} finally {
			// It's important to close the connection when you are done with it
			try {
				conn.close();
			} catch (Throwable ignore) {
				/*
				 * Propagate the original exception instead of this one that you
				 * may want just logged
				 */ }
		}

		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
    	Session session = driver.session();
    	session.run( statement );
    	session.close();
    	driver.close();
	}
}

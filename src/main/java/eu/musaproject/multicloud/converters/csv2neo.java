package eu.musaproject.multicloud.converters;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.opencsv.CSVReader;

import eu.musaproject.multicloud.utilities.NeoDriver;

public class csv2neo {
	
	public static void createGraphFromCsv (String SLAfile, String type) {
		Path p=Paths.get(SLAfile);
		String SLAname=p.getFileName().toString();
		CSVReader reader = null;
		HashMap<String,Boolean> families =new HashMap<String,Boolean>();
		try {
			reader = new CSVReader(new FileReader(SLAfile),'\t');
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String statement="CREATE ("+SLAname+":"+type+" {name:'"+SLAname+"'}),";
		statement+="(AC:ControlFamily {name:'AC', CFid:'ACCESS CONTROL'}),";
		statement+="(AC) -[:ControlFamilyIn]->("+SLAname+") ,";
		statement+="(IA:ControlFamily {name:'IA', CFid:'IDENTIFICATION AND AUTHENTICATION'}),(IA) -[:ControlFamilyIn]->("+SLAname+")";


		try {
			List<String[]> myEntries=reader.readAll();
			for (int i=0;i<myEntries.size();i++) {
				if (myEntries.get(i)[1].equalsIgnoreCase("si")) {
					String SC= myEntries.get(i)[0];
					String CF=myEntries.get(i)[0].substring(0,2);
					SC=SC.replace('-', '_');
					SC=SC.replace('(', '_');
					SC=SC.replace(')', ' ');
					
					//If this is the first control of this family, add the family to the SLA
					if(families.get(CF)!=null) {
						families.put(CF, true);
						statement=statement+",";
						statement=statement+"("+CF+":ControlFamily {name:'"+CF+"'}),";
						statement=statement+"("+CF+") -[:ControlFamilyIn]->("+SLAname+") ,";
					}

					statement=statement+",";
					statement+="("+SC+":SecurityControl {name:'"+SC+"'}),";
					statement+="("+SC+") -[:SecurityControlOf]->("+CF+")";
				}
//				System.out.println("Control: "+myEntries.get(i)[0].substring(0,2));
//				System.out.println("Value: "+myEntries.get(i)[1]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    System.out.println(statement);
	    NeoDriver.execute(statement);
	 	}

	public static void createGraphFromCsvContent (String SLAname, String SLA) {
//		Path p=Paths.get(SLAfile);
//		String SLAname=p.getFileName().toString();
		HashMap<String,Boolean> families =new HashMap<String,Boolean>();
		String statement="CREATE ("+SLAname+":SLA {name:'"+SLAname+"'}),";
		statement+="(AC:ControlFamily {name:'AC', CFid:'ACCESS CONTROL'}),";
		statement+="(AC) -[:ControlFamilyIn]->("+SLAname+") ,";
		statement+="(IA:ControlFamily {name:'IA', CFid:'IDENTIFICATION AND AUTHENTICATION'}),(IA) -[:ControlFamilyIn]->("+SLAname+")";



			String[] rows=SLA.split("\n");
			
			List<String[]> myEntries= new ArrayList<String[]>();
			for (int j=0;j<rows.length;j++){
				myEntries.add(rows[j].split("\t"));
			}
			for (int i=0;i<myEntries.size();i++) {
				System.out.println(myEntries.get(i)[1]);
				if (myEntries.get(i)[1].equalsIgnoreCase("si")) {
					System.out.println("in"+myEntries.get(i)[0]);
					String SC= myEntries.get(i)[0];
					String CF=myEntries.get(i)[0].substring(0,2);
					SC=SC.replace('-', '_');
					SC=SC.replace('(', '_');
					SC=SC.replace(')', ' ');
					
					//If this is the first control of this family, add the family to the SLA
					if(families.get(CF)!=null) {
						families.put(CF, true);
						statement=statement+",";
						statement=statement+"("+CF+":ControlFamily {name:'"+CF+"'}),";
						statement=statement+"("+CF+") -[:ControlFamilyIn]->("+SLAname+") ,";
					}

					statement=statement+",";
					statement+="("+SC+":SecurityControl {name:'"+SC+"'}),";
					statement+="("+SC+") -[:SecurityControlOf]->("+CF+")";
					System.out.println(SC);
				}
				
//				System.out.println("Control: "+myEntries.get(i)[0].substring(0,2));
//				System.out.println("Value: "+myEntries.get(i)[1]);
			}
	    System.out.println(statement);
	    NeoDriver.execute(statement);
	 	}

	public static void main (String args[]) {
		csv2neo.createGraphFromCsv("src/main/resources/SLAs/SLA_OAUTH_Google","SLA");
	}

}

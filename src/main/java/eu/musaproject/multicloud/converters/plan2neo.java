package eu.musaproject.multicloud.converters;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class plan2neo {

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static void planReader(String planFile) {
		String plan=null;
	
		try {
			plan=readFile(planFile,StandardCharsets.UTF_8);
			plan=plan.replaceAll("-","");

			planConverter(plan);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void planConverter(String planString) {
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();

		ImplementationPlanTwoProviders myplan=null;

		
		try {
			// Plan Reader
			ObjectMapper mapper = new ObjectMapper();

		    myplan = mapper.readValue(planString, ImplementationPlanTwoProviders.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int i,j,k;

		//Context INformation
		String statement ="CREATE ";
		
		//For each Provider
		for (i=0;i<myplan.getCsps().size();i++) {
			String CSPname=myplan.getCsps().get(i).getIaas().getProvider();
			statement +="("+CSPname+":CSP {name:'"+CSPname+"'}) ";
			// For each vm on such Provider
			for (j=0;j<myplan.getCsps().get(i).getPools().get(0).getVms().size();j++) {
				statement += ",";
				String vmname="vm"+i+j;
				statement += "("+vmname+":IaaS:service {name:'"+vmname+"'}), ";
				statement += "("+CSPname+") -[:provides] -> ("+vmname+")";
				//For each component add to the model
				for (k=0;k<myplan.getCsps().get(i).getPools().get(0).getVms().get(j).getComponents().size();k++) {
					statement += ",\n";
					String component=myplan.getCsps().get(i).getPools().get(0).getVms().get(j).getComponents().get(k).getComponentId();
					statement += "("+component+":service {name:'"+component+"'}), ";
					statement += "("+vmname+") -[:hosts] -> ("+component+")";
				}
			}
			
		}
		System.out.println(statement);

		session.run(statement);

		session.close();
		driver.close();

	}

	public static void deployMACM(String planString) {
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();

		ImplementationPlanTwoProviders myplan=null;

		
		try {
			// Plan Reader
			ObjectMapper mapper = new ObjectMapper();

		    myplan = mapper.readValue(planString, ImplementationPlanTwoProviders.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int i,j,k;

		//Context INformation
		String statement ="CREATE ";
		System.out.println("started");
		//For each Provider
		for (i=0;i<myplan.getCsps().size();i++) {
			String CSPname=myplan.getCsps().get(i).getIaas().getProvider();
			statement +="("+CSPname+":CSP {name:'"+CSPname+"'}) ";

			System.out.println(statement);
			session.run(statement);
			// For each vm on such Provider
			for (j=0;j<myplan.getCsps().get(i).getPools().get(0).getVms().size();j++) {
				statement = "MATCH\n";
				String vmname="vm"+i+j;
				statement += "("+CSPname+":CSP {name:'"+CSPname+"'}) ";
				statement += "CREATE ("+vmname+":IaaS:service {name:'"+vmname+"'}), ";
				statement += "("+CSPname+") -[:provides] -> ("+vmname+")";
				
				System.out.println(statement);
				session.run(statement);
				//For each component add to the model
				for (k=0;k<myplan.getCsps().get(i).getPools().get(0).getVms().get(j).getComponents().size();k++) {
					statement = "MATCH \n";
					String component=myplan.getCsps().get(i).getPools().get(0).getVms().get(j).getComponents().get(k).getComponentId();
					
					statement += "("+component+":service {name:'"+component+"'}), ";
					statement += "("+vmname+":IaaS:service {name:'"+vmname+"'}) ";
					statement += "CREATE ("+vmname+") -[:hosts] -> ("+component+")";

					System.out.println(statement);
					session.run(statement);
				}
			}
			
		}
		System.out.println(statement);

//		session.run(statement);

		session.close();
		driver.close();

	}

	public static void main(String args[]) {
		String plan=null;
		try {
			plan=readFile("src/main/resources/Models/TUTplan",StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		plan2neo.deployMACM(plan);
	}
}

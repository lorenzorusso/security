package eu.musaproject.multicloud.models.macmSLA;

import org.apache.commons.lang3.StringUtils;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.graphs.graphModel;

public class macmSLA extends graphModel{
	String itsName;
	String itsControlFramework;
	SLAPeerType type=SLAPeerType.SLA;

	SLAPeerType getType() {return type;}

	public String SetName(String Name) {
		itsName=Name;
		return itsName;
	}
	
	public macmSLA(String name, String CF) {
		super();
		this.itsName=name;
		this.itsControlFramework=CF;
		this.addPeer(itsName, type);
	}
	
	public macmSLA(String name){
		super();
		this.itsName=name;
		this.itsControlFramework="NIST_Framework";
		this.addPeer(itsName, type);		
	}
	
	public macmSLA() {
		super();
	}

	public String getName() {
		return itsName;
	}
	
	public boolean hasControl(String SC) {
		return Peers.containsKey(SC);
	}
	public void addSecurityControl(String name){
		String family=StringUtils.substringBefore(name, "_").toUpperCase();
//		System.out.println("adding "+name.toUpperCase()+" and rel to "+family);
		Peer start=addPeer(name.toUpperCase(),SLAPeerType.SecurityControl);
		Peer stop=getPeers().get(family);
		if (stop==null) {
//			System.out.println("Adding family");
			stop=addControlFamily(family);
		}
		Relationship rel= new Relationship(start,stop,SLARelationshipType.SecurityControlOf);
		Rels.add(rel);
		if(rel.endNode==null) {
			System.out.println("Error in rel");
			System.exit(-1);
		}
	};

	public Peer addControlFamily(String name){
//		System.out.println("Add CF "+name);
		Peer p=addPeer(name,SLAPeerType.ControlFamily);
		addControlFamilyIn(name,itsName);
		return p;
	};

	public void addSLO(String name){
		addPeer(name,SLAPeerType.SLO);
	};
	
	public void addMetric(String name){
		addPeer(name,SLAPeerType.SecurityMetric);
	};
	
	public void addSecurityControlOf(String SC, String CF) {
		this.addRelationship(SC, CF, SLARelationshipType.SecurityControlOf);
	}
	public void addControlFamilyIn(String CF, String SLAname) {
		this.addRelationship(CF, SLAname, SLARelationshipType.ControlFamilyIn);
	}

	public void addSLOin(String SC, String CF) {
		this.addRelationship(SC, CF, SLARelationshipType.SLOin);
	}
	public void addMeasuredWith(String SC, String CF) {
		this.addRelationship(SC, CF, SLARelationshipType.MeasuredWith);
	}
	
	public void addMappedTo(String SC, String CF) {
		this.addRelationship(SC, CF, SLARelationshipType.MappedTo);
	}
	
	public void readNeo(String SLAname) {
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();

		String statement="";

		
		
		this.itsName=SLAname;
		this.addPeer(itsName, this.getType());
		
		//statement ="MATCH (p:SLA {name:'"+SLAname+"}) return p.name AS name ";
		
		//Add All families in the sla
		statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'}), (cf)-[:ControlFamilyIn] ->(n) return cf.name AS name";
//		System.out.println(statement);
		StatementResult result=session.run(statement);
		while ( result.hasNext() )
		{
			Record record = result.next();
			String cf=record.get("name").asString();
			addControlFamily(cf);
			//addControlFamilyIn(cf,itsName);

			statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'}), (cf {name:'"+cf+"'})-[:ControlFamilyIn] ->(n), (c)-[:SecurityControlOf]->(cf) return c.name AS name";
			StatementResult result2=session.run(statement);
			while ( result2.hasNext() )
			{
				Record record2 = result2.next();
				String c=record2.get("name").asString();
				addSecurityControl(c);
				//addSecurityControlOf(c,cf);
			}
		}
		
		//Add SLOs
		statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'}), (slo)-[:SLOin] ->(n) return slo.name AS name";
		StatementResult result3=session.run(statement);

		while ( result3.hasNext() )
		{
			Record record = result3.next();
			String slo=record.get("name").asString();
			addSLO(slo);
			addSLOin(slo,itsName);

			statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'}), (slo {name:'"+slo+"'})-[:SLOin] ->(n), (slo)-[:MeasuredWith]->(sm) return sm.name AS name";
			StatementResult result4=session.run(statement);
			while ( result4.hasNext() )
			{
				Record record2 = result4.next();
				String sm=record2.get("name").asString();
				addMetric(sm);
				addMeasuredWith(slo,sm);
				
				statement ="match (n:"+this.getType()+" {name:'"+SLAname+"'}), (slo {name:'"+slo+"'})-[:SLOin] ->(n), (slo)-[:MeasuredWith]->(sm {name:'"+sm+"'}), (sm) -[:Mappedto]->(c) return c.name AS control";
				StatementResult result5=session.run(statement);
				System.out.println(statement);
				while ( result5.hasNext() )
				{
					Record record3 = result5.next();
					String c=record3.get("control").asString();
					addMappedTo(sm,c);
				}
			}
		}
		
		
		session.close();
		driver.close();


	}
	
	static public void main(String args[]){
		macmSLA sla=new macmSLA();
		sla.readNeo("csla");
		sla.print();
	}
}

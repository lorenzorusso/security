package eu.musaproject.multicloud.models.SLA;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;

import eu.musaproject.multicloud.utilities.NeoDriver;

public class SLA {
	Map<String, Control> Controls;
	List<Link> Rels;
	String SLAname;
	CFmap myControlFramework;
	Driver neo; 


	public SLA(String name) {
		Controls=new HashMap<String, Control>();
		Rels= new ArrayList<Link>();
		SLAname=name;
		myControlFramework=new NISTmap();
		neo = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
	}
	
	public void finalize() {
		neo.close();		
	}

	public void addSC(String SC[], int level ){
		// Security Controls can be of different level, according to the Frameworks adopted. 
		// Only leafs are added. Upper layers are added consequently
		// NIST Layers: Family: 1, Control: 2, Control Enhancement: 3
		// Representation: SC[level-1]: ID of level,
		Control t=null;
		for (int i=0;i<level;i++) {
			t=Controls.get(SC[i]); 
			if (t==null) {
				System.out.println("SC:"+myControlFramework.Controlevels.get(i));
				t= new Control(SC[i],myControlFramework.Controlevels.get(i));
			}
		}
		Controls.put(t.getName(), t);
	}

	public void readSLAfromDB() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "stevens");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Statement stmt = null;
			stmt = conn.createStatement();

			ResultSet rs;
			//Generate Security Controls
			rs = stmt.executeQuery("select idControl,familyId from controls ");

			int level=0;
			String SC[]=null;
			while (rs.next()) {
				String SCstring=(String) rs.getObject(1);
				if (SCstring.contains("(")) {
					String tmp[];
					level=2; 
					tmp=SCstring.split("-");
					SC[0]=tmp[0];
					tmp=tmp[1].split("(");	
					SC[1]=tmp[0];
					SC[2]=tmp[1];
				} else {
					level=1;
					SC=SCstring.split("-");
				}
				System.out.println(" SC:"+SCstring+" level:"+level);
				addSC(SC, level);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addGraphSC(String SC,Session neos){
		String statement=null;
		
		//TODO Add relationship to family
		statement="CREATE \n";
		statement+=" ("+SC+":control)";
		neos.run(statement);
	}
	
	public void addSLA(String SLA,Session neos){
		String statement=null;
		statement="CREATE \n";
		statement+=" ("+SLA+":name)";
		neos.run(statement);
	}
	
	public void createNeo() {
		Session neos=neo.session();
		addSLA(SLAname,neos);
		
		//Create all the Peers
		for (Map.Entry<String, Control> entry : Controls.entrySet()){
			//System.out.println(entry.getKey() + "/" + entry.getValue());
			Control c=entry.getValue();
			addGraphSC(c.getName(),neos);
		}

		neos.close();
	}
	
	public static void main(String Args[]) {
		SLA sla= new SLA("NIST");
		sla.readSLAfromDB();
		sla.createNeo();
	}
}

package eu.musaproject.multicloud.models.SLA;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import java.util.Map.Entry;
import java.util.Set;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.utilities.NeoDriver;

public class SLAgraphs {
	
	public static void missingSLA() {
//			MATCH (n:service) OPTIONAL MATCH (n)-[r:SLA]->() WITH  n,r WHERE r is null return n,r

		String statement="MATCH (n:service)";		
		statement+=" OPTIONAL MATCH (n)-[r:SLA]->() "; 
		statement+="WITH  n,r WHERE r is null ";
		statement+="return n.name AS name";

		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
    	Session session = driver.session();
    	StatementResult result =session.run( statement );
    	
    	while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    System.out.println(record.get("name").asString() );
    	}
    	
    	session.close();
    	driver.close();

		//System.out.println(statement);
	}
	
	public static void SLAs() {
//		MATCH (n:service) OPTIONAL MATCH (n)-[r:SLA]->() WITH  n,r WHERE r is null return n,r

	String statement="MATCH (n:SLA) ";		
	statement+="return n.name AS name ";

	Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
	Session session = driver.session();
	StatementResult result =session.run( statement );
	
	while ( result.hasNext() )
	{
	    Record record = result.next();
	    System.out.println(record.get("name"));
	}
	
	session.close();
	driver.close();

	//System.out.println(statement);
}
	
	public static void ControlsWithoutMetric(String sla) {
		//match (n:SecurityControl) optional match ()-[r:MetricMappedTo]->(n) WITH n,r where r is null return n,r

		String statement="MATCH (n:SecurityControl)";		
		statement+=" OPTIONAL MATCH ()-[r:MetricMappedTo]->(n) "; 
		statement+="WITH  n,r WHERE r is null ";
		statement+="return n.name AS name";

		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "maxrak"));
    	Session session = driver.session();
    	StatementResult result =session.run( statement );
    	
    	while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    System.out.println(record.get("name").asString() );
    	}
    	
    	session.close();
    	driver.close();

	}
	
	public static void composeSLA(String SLA1, String SLA2, String composeSLAname){
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
    	Session session = driver.session();

		StatementResult AllControls=SLAgraphs.Controls("NISTFramework");
		StatementResult SLA1Controls=SLAgraphs.Controls(SLA1);
		StatementResult SLA2Controls=SLAgraphs.Controls(SLA2);
		StatementResult composedSLOs=SLAgraphs.ComposedSlos(SLA1, SLA2);
//		StatementResult SLA1Metrics=SLAgraphs.Metrics(SLA1);
//		StatementResult SLA2Metrics=SLAgraphs.Metrics(SLA2);
		
		HashMap<String,String> SLA2map =new HashMap<String,String>();
		HashMap<String,String> SLA1map =new HashMap<String,String>();
		HashMap<String,String> composeSLAmap =new HashMap<String,String>();
		HashMap<String,Boolean> families =new HashMap<String,Boolean>();
		
//		HashMap<String,Set<String>> map2ctrl1=SLAgraphs.metric2control(SLA1);
//		HashMap<String,Set<String>> map2ctrl2=SLAgraphs.metric2control(SLA2);
//		HashMap<String,Set<String>> map2ctrlComposed=new HashMap<String,Set<String>>();
		
		String statement="CREATE ("+composeSLAname+":SLA {name:'"+composeSLAname+"'}) ";

		//All Controls in First SLA
		while ( SLA1Controls.hasNext() )
		{
		    Record SCrecord = SLA1Controls.next();
		    SLA1map.put(SCrecord.get("name").asString(),"si");
		 }

		//All Controls in Second SLA
		while ( SLA2Controls.hasNext() )
		{
		    Record SCrecord = SLA2Controls.next();
		    SLA2map.put(SCrecord.get("name").asString(),"si");
		 }

		//Compose the controls according to their criteria
		//Note: at state of art composition is always min
		while ( AllControls.hasNext() )
		{
		    Record SCrecord = AllControls.next();
		    if (SLA1map.containsKey(SCrecord.get("name").asString())&&SLA2map.containsKey(SCrecord.get("name").asString())) {
		    	composeSLAmap.put(SCrecord.get("name").asString(), "si");
		    }			    
		 }
		
		for(Entry<String, String> entry : composeSLAmap.entrySet()) {		    
			String SC= entry.getKey();
			String CF=SC.substring(0,2);
			
			//If this is the first control of this family, add the family to the SLA
			if(families.get(CF)==null) {
				families.put(CF, true);
				statement=statement+",";
				statement=statement+"("+CF+":ControlFamily {name:'"+CF+"'}),";
				statement=statement+"("+CF+") -[:ControlFamilyIn]->("+composeSLAname+")";
			}

			statement=statement+",";
			statement+="("+SC+":SecurityControl {name:'"+SC+"'}),";
			statement+="("+SC+") -[:SecurityControlOf]->("+CF+") ";
		}
	
		Record sloRecord=null;
		Set<String> slos=new HashSet<String>();
		Set<String> metrics=new HashSet<String>();
		while (composedSLOs.hasNext()) {
			//add SLO and 
			sloRecord=composedSLOs.next();
			if (!(metrics.contains(sloRecord.get("metric").asString()))) {
				//First time I meet this metric, create it.
				metrics.add(sloRecord.get("metric").asString());
				statement=statement+",";
				statement+="("+sloRecord.get("metric").asString()+":SecurityMetric {name:'"+sloRecord.get("metric").asString()+"'}) ";
			}
			if (!(slos.contains(sloRecord.get("name").asString()))) {
				//First time I meet this slo, create it.
				slos.add(sloRecord.get("name").asString());
				statement=statement+",";
				statement+="("+sloRecord.get("name").asString()+":SLO {name:'"+sloRecord.get("name").asString()+"'}), ";
				statement+="("+sloRecord.get("name").asString()+")-[:SLOin]->("+composeSLAname+"), ";
				statement+="("+sloRecord.get("name").asString()+")-[:MeasuredWith]->("+sloRecord.get("metric").asString()+"), ";
				statement+="("+sloRecord.get("metric").asString()+")-[:Mappedto]->("+sloRecord.get("control").asString()+") ";
			} else {
				//Control and Metric already added... just add the additional mapping
				statement=statement+",";
				statement+="("+sloRecord.get("metric").asString()+")-[:Mappedto]->("+sloRecord.get("control").asString()+") ";
			}
				
		}
		
		System.out.println(statement);
		session.run(statement);
		
		session.close();
		driver.close();
	}
	
	public static void ComposeSLAold(String SLA1, String SLA2, String composeSLAname){
		//String composeSLAname="composedSLA";
		
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "maxrak"));
		Session session = driver.session();
		
		StatementResult Allfamilies;
		StatementResult AllControls;
		StatementResult SLA1Controls;
		StatementResult SLA2Controls;
//		StatementResult SLA1slos;
//		StatementResult SLA2slos;
		
		HashMap<String,String> SLA2map =new HashMap<String,String>();
		HashMap<String,String> SLA1map =new HashMap<String,String>();
		HashMap<String,String> composeSLAmap =new HashMap<String,String>();
		String statement="MATCH (n:SLA {name:'NISTFramework'}), (CF)-[:ControlFamilyIn]->(n) ";		
		statement+="return CF.name AS name ";

		Allfamilies =session.run( statement );

//		statement="MATCH (n:SLA {name:'"+SLA1+"'}), (CF)-[:ControlFamilyIn]->(n) ";		
//		statement+="return CF.name AS name ";
//
//		SLA1families =session.run( statement );
//
//		statement="MATCH (n:SLA {name:'"+SLA2+"'}), (CF)-[:ControlFamilyIn]->(n) ";		
//		statement+="return CF.name AS name ";
//
//		SLA2families =session.run( statement );
				
		while ( Allfamilies.hasNext() )
		{
		    Record record = Allfamilies.next();
		    
		    //All Controls in this Family
		    statement="MATCH (n:SLA {name:'NISTFramework'}), (CF)-[:ControlFamilyIn]->(n), ";
		    statement+="(SC)-[:SecurityControlOf]->("+record.get("name").asString()+")";
			statement+="return SC.name AS name ";

			AllControls =session.run( statement );

			//Controls in SLA1 for this family
			statement="MATCH (n:SLA {name:'"+SLA1+"'}), (CF)-[:ControlFamilyIn]->(n), ";
		    statement+="(SC)-[:SecurityControlOf]->(CF)";
			statement+="WHERE CF.name='"+record.get("name").asString()+"' ";
			statement+="return SC.name AS name ";

			SLA1Controls =session.run( statement );
			while ( SLA1Controls.hasNext() )
			{
			    Record SCrecord = SLA1Controls.next();
			    SLA1map.put(SCrecord.get("name").asString(),"si");
			 }

			//Controls in SLA2 for this family
			statement="MATCH (n:SLA {name:'"+SLA2+"'}), (CF)-[:ControlFamilyIn]->(n), ";
		    statement+="(SC)-[:SecurityControlOf]->(CF)";
			statement+="WHERE CF.name='"+record.get("name").asString()+"' ";
		    statement+="return SC.name AS name ";

			SLA2Controls =session.run( statement );
			while ( SLA2Controls.hasNext() )
			{
			    Record SCrecord = SLA2Controls.next();
			    SLA2map.put(SCrecord.get("name").asString(),"si");
			 }
			
			while ( AllControls.hasNext() )
			{
			    Record SCrecord = AllControls.next();
			    if (SLA1map.containsKey(SCrecord.get("name").asString())&&SLA2map.containsKey(SCrecord.get("name").asString())) {
			    	composeSLAmap.put(SCrecord.get("name").asString(), "si");
			    }			    
			 }
		}
		
//		//Select the SLOs in SLA1
//		statement="MATCH (n:SLA {name:'"+SLA1+"'}), (slo)-[:SLOIn]->(n), ";
//	    statement+="return slo.name AS name ";
//	    SLA1slos=session.run(statement);
		
		HashMap<String,Boolean> families =new HashMap<String,Boolean>();
		statement="CREATE ("+composeSLAname+":SLA {name:'"+composeSLAname+"'}) ";

		for(Entry<String, String> entry : composeSLAmap.entrySet()) {
   
			String SC= entry.getKey();
			String CF=SC.substring(0,2);
			
			//If this is the first control of this family, add the family to the SLA
			if(families.get(CF)==null) {
				families.put(CF, true);
				statement=statement+",";
				statement=statement+"("+CF+":ControlFamily {name:'"+CF+"'}),";
				statement=statement+"("+CF+") -[:ControlFamilyIn]->("+composeSLAname+")";
			}

			statement=statement+",";
			statement+="("+SC+":SecurityControl {name:'"+SC+"'}),";
			statement+="("+SC+") -[:SecurityControlOf]->("+CF+") ";
		    
		}
				
		System.out.println(statement);
		session.run(statement);
		
		session.close();
		driver.close();
	}
	
	public static void addComponentSLA(String service, String SLAname) {
		String statement="MATCH (s:service {name:'"+service+"'}),";		
		statement+="(sla:SLA {name:'"+SLAname+"'})"; 
		statement+="CREATE (s)-[:componentSLA]->(sla)";
		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addSLA(String service, String SLAname) {
		String statement="MATCH (s:service {name:'"+service+"'}),";		
		statement+="(sla:SLA {name:'"+SLAname+"'})"; 
		statement+="CREATE (s)-[:SLA]->(sla)";
		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addSLAT(String service, String SLAname) {
		String statement="MATCH (s:service {name:'"+service+"'}),";		
		statement+="(sla:SLA {name:'"+SLAname+"'})"; 
		statement+="CREATE (s)-[:supports]->(sla)";
		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static StatementResult Controls(String SLA){
	//match (sla:SLA {name:'csla'}), (ctrl)-[:SecurityControlOf]->()-[:ControlFamilyIn]->(sla) return ctrl
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();
		
		StatementResult AllControls=null;
		
		String statement="MATCH (sla:SLA {name:'"+SLA+"'}), (ctrl)-[:SecurityControlOf]->()-[:ControlFamilyIn]->(sla) ";		
		statement+="return ctrl.name AS name ";

//		System.out.println(statement);
		AllControls =session.run( statement );

		session.close();
		driver.close();
		
		return AllControls;
		
	}

	public static StatementResult Metrics(String SLA){
	//match (sla:SLA {name:'WebPool_SVA'}), (metric)<-[:MeasuredWith]-()-[:SLOin]->(sla) return metric
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();
		
		StatementResult AllControls=null;
		
		String statement="MATCH (sla:SLA {name:'"+SLA+"'}), (metric)<-[:MeasuredWith]-()-[:SLOin]->(sla) ";		
		statement+="return metric.name AS name ";

//		System.out.println(statement);
		AllControls =session.run( statement );

		session.close();
		driver.close();
		
		return AllControls;
		
	}

	public static StatementResult Slos(String SLA){
	//match (sla:SLA {name:'WebPool_SVA'}), (metric)<-[:MeasuredWith]-()-[:SLOin]->(sla) return metric
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();
		
		StatementResult AllSLOs=null;
		
		String statement="match (sla:SLA {name:'"+SLA+"'}), (slo)-[:SLOin]->(sla), (slo)-[:MeasuredWith]->(metric) ";		
		statement+="return slo.name AS name, slo.operator AS operator, slo.value as value, metric.name AS metric   ";

		System.out.println(statement);
		AllSLOs =session.run( statement );

		session.close();
		driver.close();
		
		return AllSLOs;
		
	}

	
	public static StatementResult ComposedSlos(String SLA1, String SLA2){

		//OLD Query
		//		match (sla:SLA {name:'WebPool_SVA'}), (slo)-[:SLOin]->(sla),(slo)-[:MeasuredWith]->(metric) match (sla2:SLA {name:'WebPool_SVA2'}), (slo2)-[:SLOin]->(sla2),(slo2)-[:MeasuredWith]->(metric2) where metric.name=metric2.name return slo.name AS name, slo.operator AS operator1,slo2.operator AS operator2, slo.value AS value1, slo2.value AS value2

		//query with SLO, metrics and mappings.
		//match (sla:SLA {name:'WebPool_SVA'}), (slo)-[:SLOin]->(sla),(slo)-[:MeasuredWith]->(metric),(metric)-[:MetricMappedTo]->(ctrl) match (sla2:SLA {name:'WebPool_SVA2'}), (slo2)-[:SLOin]->(sla2),(slo2)-[:MeasuredWith]->(metric2),(metric2)-[:MetricMappedTo]->(ctrl2) where metric.name=metric2.name AND ctrl.name=ctrl2.name return DISTINCT slo.name AS name, slo.operator AS operator1,slo2.operator AS operator2, slo.value AS value1, slo2.value AS value2, metric.name AS metric, ctrl.name AS contrl
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();
		
		StatementResult AllSLOs=null;
		
		String statement="match (sla:SLA {name:'"+SLA1+"'}), (slo)-[:SLOin]->(sla),(slo)-[:MeasuredWith]->(metric),(metric)-[:MetricMappedTo]->(ctrl) ";
		statement+="match (sla2:SLA {name:'"+SLA2+"'}), (slo2)-[:SLOin]->(sla2),(slo2)-[:MeasuredWith]->(metric2),(metric2)-[:MetricMappedTo]->(ctrl2)";
		statement+="where metric.name=metric2.name AND ctrl.name=ctrl2.name ";
		statement+="return DISTINCT slo.name AS name, slo.operator AS operator1,slo2.operator AS operator2, slo.value AS value1, slo2.value AS value2, metric.name AS metric, ctrl.name AS control";

		System.out.println(statement);
		AllSLOs =session.run( statement );

		session.close();
		driver.close();
		
		return AllSLOs;
		
	}

	public static StatementResult Families(String SLA){
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();
		
		StatementResult Allfamilies=null;
		
		String statement="MATCH (n:SLA {name:'"+SLA+"'}), (CF)-[:ControlFamilyIn]->(n) ";		
		statement+="return CF.name AS name ";

		Allfamilies =session.run( statement );
		
		session.close();
		driver.close();
		
		return Allfamilies;
		
	}

	public static HashMap<String,Set<String>> metric2control(String SLA){
		//TODO passare da hashmap a hashset
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session = driver.session();
		HashMap<String,Set<String>> maplist= new HashMap<String,Set<String>>();
		HashMap<String,String> map=new HashMap<String,String> ();
		StatementResult Mresult=null;
		StatementResult Cresult=null;
		String metric;
		String ctrl;
		String statement="MATCH (n:SLA {name:'"+SLA+"'}), (ctrl)<-[:MetricMappedTo]-(metric)<-[:MeasuredWith]-()-[:SLOin]->(sla)  ";		
		statement+="return metric.name AS name ";

		Mresult =session.run( statement );
		while ( Mresult.hasNext() )
		{
		    Record Mrecord = Mresult.next();
		    metric=Mrecord.get("name").asString();

			statement="MATCH (sla:SLA {name:'"+SLA+"'}), (ctrl)<-[:MetricMappedTo]-(metric {name:'"+metric+"'})<-[:MeasuredWith]-()-[:SLOin]->(sla)  ";		
			statement+="return ctrl.name as name ";

			System.out.println(statement);
			Cresult =session.run( statement );
			List <Record> crecords=Cresult.list();
			//System.out.println("maps "+crecords.size());
			
			for (int i=0;i<crecords.size();i++) {
				ctrl=crecords.get(i).get("name").asString();
				Set<String> ctrls=maplist.get(metric);
				if (ctrls==null) {
					ctrls=new HashSet<String>();
					ctrls.add(ctrl);
					maplist.put(metric, ctrls);
				} else {
					ctrls.add(ctrl);
				}
			}
		 }

		
		session.close();
		driver.close();
		
		return maplist;
		
	}

	public static void PrintProperties(StatementResult sr,String property){
		while ( sr.hasNext() )
		{
		    Record SCrecord = sr.next();
		    System.out.println(SCrecord.get(property));
		 }

	}

	public static void PrintMaps (HashMap<String,Set<String>> map) {
		for(Entry<String, Set<String>> entry : map.entrySet()) {
			Set<String> s=entry.getValue();
			System.out.print("Metric:"+entry.getKey());
			Iterator<String> it=s.iterator();
			while (it.hasNext()) {
				String ctrl=it.next();
				System.out.print(" "+ctrl);	
			}
			System.out.println();
		}
		
	}
	public static void main (String args[]) {
//		String SLAfile="src/main/resources/SLAs/SLA_OAUTH_Google";
//		SLAgraphs.addComponentSLA("IDMGoogle", "SLA_OAUTH_Google");
//		SLAgraphs.ComposeSLA("SLA_TUT_JP", "SLA_VM_Ubuntu", "xx");
//		SLAgraphs.PrintFamilies("WebPool_SVA");
//		SLAgraphs.ControlsWithoutMetric("WebPool_SVA");
//		SLAgraphs.PrintProperties(SLAgraphs.Slos("WebPool_SVA"),"name");
//		SLAgraphs.composeSLA("WebPool_SVA", "WebPool_SVA2", "csla2");
//		SLAgraphs.PrintMaps(SLAgraphs.metric2control("WebPool_SVA"));
//		SLAgraphs.PrintProperties(SLAgraphs.Slos("WebPool_SVA"),"name");
//		SLAgraphs.PrintProperties(SLAgraphs.Slos("WebPool_SVA"),"metric");
		SLAgraphs.PrintProperties(SLAgraphs.ComposedSlos("WebPool_SVA", "WebPool_SVA2"), "name");
	}

}

package eu.musaproject.multicloud.models.SLA;

public enum LinkType {
    hosts,
    uses,
    provides,
    owns,
    supports,
    grants
}

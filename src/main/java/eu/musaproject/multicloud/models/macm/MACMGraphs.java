package eu.musaproject.multicloud.models.macm;



import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.utilities.NeoDriver;

public class MACMGraphs {
	
	public static void addServiceIaaS(String service) {
		String statement="CREATE (s:service:IaaS {name:'"+service+", type:'vm'})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addServiceIaaS(String service, String type) {
		String statement="CREATE (s:service:IaaS {name:'"+service+", type:'"+type+"'})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addServicePaaS(String service) {
		String statement="CREATE (s:service:PaaS {name:'"+service+"})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addServicePaaS(String service, String type) {
		String statement="CREATE (s:service:PaaS {name:'"+service+", type:'"+type+"'})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addServiceSaaS(String service) {
		String statement="CREATE (s:service:SaaS {name:'"+service+"})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addServiceSaaS(String service, String type) {
		String statement="CREATE (s:service:SaaS {name:'"+service+", type:'"+type+"'})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}

	public static void addCSP(String csp) {
		String statement="CREATE (s:CSP {name:'"+csp+"})";		
		//System.out.println(statement);
		NeoDriver.execute(statement);
	}
	
	public static void provide(String csp, String service) {
		String statement="MATCH (s:service {name:'"+service+"'}),";		
		statement+="(csp:CSP {name:'"+csp+"'})"; 
		statement+="CREATE (csp)-[:provides]->(service)";
		NeoDriver.execute(statement);
	}

	public static void host(String hosting, String service) {
		//TODO Add Exception in case the hosting service for some reason cannot have the host relationship
		String statement="MATCH (s:service {name:'"+service+"'}),";		
		statement+="(hosting:service {name:'"+hosting+"'})"; 
		statement+="CREATE (csp)-[:hosts]->(service)";
		NeoDriver.execute(statement);
	}

	public static void uses(String caller, String callee) {
		//TODO Add Exception in case the hosting service for some reason cannot have the host relationship
		String statement="MATCH (caller:service {name:'"+caller+"'}),";		
		statement+="(callee:service {name:'"+callee+"'})"; 
		statement+="CREATE (caller)-[:uses]->(callee)";
		NeoDriver.execute(statement);
	}

	public static void CSPs() {
		//TODO Add Exception in case the hosting service for some reason cannot have the host relationship
		String statement="MATCH (csp:CSP)";		
		statement+="return csp.name AS name";
		Driver driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "maxrak"));

		Session session = driver.session();
		StatementResult result =session.run( statement );
		
		while ( result.hasNext() )
		{
		    Record record = result.next();
		    System.out.println(record.get("name"));
		}
		
		session.close();
		driver.close();
	}

	public static void main(String args[]) {
		MACMGraphs.CSPs();
	}
}

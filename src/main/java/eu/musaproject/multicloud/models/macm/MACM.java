package eu.musaproject.multicloud.models.macm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.graphs.graphModel;

public class MACM extends graphModel{
	
	public MACM() {
		setPeers(new HashMap<String, Peer>());
		Rels= new ArrayList<Relationship>();
	}

	public Peer addIaaService(String name){
		Peer p= new Peer(name,macmPeerType.IaaS);
		getPeers().put(name,p);
		return p;
	};

	public Peer addPaaService(String name){
		Peer p= new Peer(name,macmPeerType.PaaS);
		getPeers().put(name,p);
		return p;
	};
	public Peer addSaaService(String name){
		Peer p= new Peer(name,macmPeerType.SaaS);
		getPeers().put(name,p);
		return p;
	};
	
	public Peer addCSP(String name){
		Peer p= new Peer(name,macmPeerType.CSP);
		getPeers().put(name,p);
		return p;
	};
	
	public Peer addCSC(String name){
		Peer p= new Peer(name,macmPeerType.CSC);
		getPeers().put(name,p);
		return p;
	}
	
	public Peer addSLA(String name){
		return this.addPeer(name, macmPeerType.SLA);
	}

	public Peer addSLAT(String name){
		return this.addPeer(name, macmPeerType.SLAT);
	}

	public Relationship addProvides(String csp, String service) {
		Peer start=getPeers().get(csp);
		Peer stop=getPeers().get(service);
		Relationship rel= new Relationship(start,stop,macmRelationshipType.provides);
		Rels.add(rel);
		return rel;

	}

	public Relationship addHosts(String hosting, String hosted) {
		Peer start=getPeers().get(hosting);
		Peer stop=getPeers().get(hosted);
		Relationship rel= new Relationship(start,stop,macmRelationshipType.hosts);
		Rels.add(rel);		
		return rel;
	}

	public Relationship addUses(String caller, String called) {
		Peer start=getPeers().get(caller);
		Peer stop=getPeers().get(called);
		Relationship rel= new Relationship(start,stop,macmRelationshipType.uses);
		Rels.add(rel);
		return rel;
	}

	public Relationship addGrants(String provider, String sla) {
		return this.addRelationship(provider, sla, macmRelationshipType.grants);
	}

	public Relationship addSupports(String provider, String slat) {
		return this.addRelationship(provider, slat, macmRelationshipType.supports);
	}

	public Relationship addRequires(String provider, String slat) {
		return this.addRelationship(provider, slat, macmRelationshipType.requires);
	}
	
	public void syncNeoRelationship(Relationship Rel) {
		Session session = driver.session();
		
		String statement="match (n:"+Rel.startNode.getType()+" {name:'"+Rel.startNode.getName()+"'}), (sla:"+Rel.endNode.getType()+" {name:'"+Rel.endNode.getName()+"'}) create (n) -[:"+Rel.type+"]->(sla)";
		System.out.println("sync:"+statement);
		session.run(statement);

		session.close();
	}
	
	public void syncNeoPeer(Peer p) {
		Session session = driver.session();
		
		String statement="create (p:"+p.type+" {name:'"+p.getName()+"'})";
		System.out.println("sync:"+statement);
		session.run(statement);

		session.close();
	}
	
	public void readNeo(){
			Session session = driver.session();
			Peer p;
			String statement="";
			StatementResult result=null;
			//Create CSP peers
			statement ="MATCH (csp:CSP) return csp.name AS name,csp.app_id AS app_id  ";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    p=addCSP(record.get("name").asString());
	    	    List<String> properties=record.keys();
	    	    for (String el: properties) {
	    	    		p.setProperty(el, record.get(el).asString());	    	    		
	    	    }
	//    	    System.out.println(record.get("name").asString() );
	    	}
			
			//Create IaaS peers
			statement ="MATCH (s:IaaS:service) return s.name AS name,s.app_id AS app_id ";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    p=addIaaService(record.get("name").asString());
	    	    List<String> properties=record.keys();
	    	    for (String el: properties) {
	    	    		p.setProperty(el, record.get(el).asString());
	    	    }
	//    	    System.out.println(record.get("name").asString() );
	    	}
	
			//Create PaaS peers
			statement ="MATCH (s:PaaS:service) return s.name AS name,s.app_id AS app_id  ";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    p=addPaaService(record.get("name").asString());
	    	    List<String> properties=record.keys();
	    	    for (String el: properties) {
	    	    		p.setProperty(el, record.get(el).asString());
	    	    }
	//    	    System.out.println(record.get("name").asString() );
	    	}
	
			//Create SaaS peers
			statement ="MATCH (s:SaaS:service) return s.name AS name,s.app_id AS app_id,s.component_id AS component_id,s.type AS type  ";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    p=addSaaService(record.get("name").asString());
	    	    List<String> properties=record.keys();
	    	    for (String el: properties) {
	    	    		p.setProperty(el, record.get(el).asString());
	    	    }
	//    	    System.out.println(record.get("name").asString() );
	    	}
			
			//Create Provide relationships
			statement ="MATCH (csp) -[r:provides]->(s) return csp.name AS csp, s.name AS service, labels(s) AS servicetype";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    addProvides(record.get("csp").asString(),record.get("service").asString());
	    	}	
			
			//Create Provide relationships
			statement ="MATCH (hosting) -[:hosts]->(s) return hosting.name AS hosting, s.name AS hosted, labels(s) AS servicetype";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    addHosts(record.get("hosting").asString(),record.get("hosted").asString());
	    	}	
	
			//Create Provide relationships
			statement ="MATCH (caller) -[:uses]->(s) return caller.name AS caller, s.name AS service, labels(s) AS servicetype";
			result=session.run(statement);
			while ( result.hasNext() )
	    	{
	    	    Record record = result.next();
	    	    addUses(record.get("caller").asString(),record.get("service").asString());
	    	}	
	
			//TODO: SLAs and SLATs
			
			session.close();
		}

	public void readNeo(String app_id){
		Session session = driver.session();
		Peer p;
		String statement="";
		StatementResult result=null;
		//Create CSP peers
		statement ="MATCH (csp:CSP {app_id:'"+app_id+"'} ) return csp.name AS name,csp.app_id AS app_id  ";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addCSP(record.get("name").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	    	    		
    	    }
//    	    System.out.println(record.get("name").asString() );
    	}
		
		//Create IaaS peers
		statement ="MATCH (s:IaaS:service {app_id:'"+app_id+"'}) return s.name AS name,s.app_id AS app_id ";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addIaaService(record.get("name").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());
    	    }
//    	    System.out.println(record.get("name").asString() );
    	}

		//Create PaaS peers
		statement ="MATCH (s:PaaS:service {app_id:'"+app_id+"'}) return s.name AS name,s.app_id AS app_id  ";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addPaaService(record.get("name").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());
    	    }
//    	    System.out.println(record.get("name").asString() );
    	}

		//Create SaaS peers
		statement ="MATCH (s:SaaS:service {app_id:'"+app_id+"'}) return s.name AS name,s.app_id AS app_id,s.component_id AS component_id,s.type AS type  ";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addSaaService(record.get("name").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());
    	    }
//    	    System.out.println(record.get("name").asString() );
    	}
		
		//Create Provide relationships
		statement ="MATCH (csp) -[r:provides]->(s) return csp.name AS csp, s.name AS service, labels(s) AS servicetype";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addProvides(record.get("csp").asString(),record.get("service").asString());
    	}	
		
		//Create Provide relationships
		statement ="MATCH (hosting) -[:hosts]->(s) return hosting.name AS hosting, s.name AS hosted, labels(s) AS servicetype";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addHosts(record.get("hosting").asString(),record.get("hosted").asString());
    	}	

		//Create Provide relationships
		statement ="MATCH (caller) -[:uses]->(s) return caller.name AS caller, s.name AS service, labels(s) AS servicetype";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addUses(record.get("caller").asString(),record.get("service").asString());
    	}	

		//TODO: SLAs and SLATs
		
		session.close();
	}

	public void writeNeo(){
		Session session = driver.session();
		int i=0;
		String statement="CREATE \n";
		

		//Create all the Peers
		for (Map.Entry<String, Peer> entry : getPeers().entrySet()){
			//System.out.println(entry.getKey() + "/" + entry.getValue());
			if(i!=0) {
				statement+=",\n";
			} else i++;
			Peer p=entry.getValue();
			statement +="("+p.getName()+":"+p.type.toString()+"{name:'"+p.getName()+"'})";
		}
		//Create all Relationships
		for (Relationship rel: Rels ){
			statement+=",\n";
			statement +="("+rel.startNode.getName()+") -[:"+rel.type.toString()+"]-> ("+rel.endNode.getName()+")";
		}
//		System.out.println(statement);
		session.run(statement);
		session.close();
	}

	
	static public void main(String Args[]){
		System.out.println("started");
		MACM macm= new MACM();
		macm.readNeo();
		for (Map.Entry<String, Peer> entry: macm.getPeers().entrySet()){
			if (entry.getValue().getType()==macmPeerType.SaaS) {
				System.out.println("Peers:"+entry.getValue().getName());
			}
		}
		
	}
}

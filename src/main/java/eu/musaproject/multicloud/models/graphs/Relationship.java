package eu.musaproject.multicloud.models.graphs;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Relationship {
	public Peer startNode;
	public Peer endNode;
	public TypeInterface type;
	Properties property=new Properties();
	Map<String,String> properties;
	
	public Relationship() {
		properties=new HashMap<String,String>();
	}
	
	public Relationship(Map<String,String> props) {
		properties=props;
	}
	
	public Relationship(Peer start,Peer end, TypeInterface t) {
		startNode=start;
		endNode=end;
		type=t;
		properties=new HashMap<String,String>();
	}
	
	public TypeInterface getType() {
		return type;
	}
	
	public Peer getStartNode() {
		return startNode;
	}
	
	public Peer getEndNode() {
		return endNode;
	}
	
	public void setProperty(String key, String value) {
		
		property.put(key, value);
	}
	
	public Properties getProperty() {
		return property;
	}

}

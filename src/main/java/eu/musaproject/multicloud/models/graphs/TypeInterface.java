package eu.musaproject.multicloud.models.graphs;

import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;

public interface TypeInterface {
	String getName();
	
	 TypeInterface isInType(String t);

}


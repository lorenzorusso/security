package eu.musaproject.multicloud.models.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.EnumUtils;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;



public class graphModel {
	protected Driver driver;

	protected  Map<String, Peer> Peers;
	protected  List<Relationship> Rels;

	public graphModel() {
		driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));

		setPeers(new HashMap<String, Peer>());
		Rels= new ArrayList<Relationship>();
	}
	
	public List<Peer> getPeersByType(TypeInterface t) {
		List<Peer> peers=new ArrayList<Peer>();
		for (Map.Entry<String, Peer> entry : Peers.entrySet())
		{
			if (entry.getValue().getType()==t) peers.add(entry.getValue());
		}
		return peers;
	}
	
	public List<Peer> getPeersByType(TypeInterface t,TypeInterface t2) {
		List<Peer> peers=new ArrayList<Peer>();
		for (Map.Entry<String, Peer> entry : Peers.entrySet())
		{
			if (entry.getValue().getType()==t) peers.add(entry.getValue());
			else if (entry.getValue().getType()==t2) peers.add(entry.getValue());
		}
		return peers;
	}
	public void close() {
		driver.close();
	}

	public Map<String, Peer> getPeers() {
		return Peers;
	}
	
	public List<Relationship> getRelationship() {
		return Rels;
	}

	public void setPeers(Map<String, Peer> peers) {
		Peers = peers;
	}

	public Peer addPeer(String name,TypeInterface t){
		Peer p= new Peer(name,t);
		Peers.put(name,p);
		return p;
	};

	public Peer addPeer(String name, String type) {
		return null;
		//TODO

	}

	public Relationship addRelationship(String Peer1, String Peer2, TypeInterface t) {
		Peer start=getPeers().get(Peer1);
		Peer stop=getPeers().get(Peer2);
		if(start==null) {
			System.out.println("Unknown start ("+Peer1+") rel!!");
		} else if (stop==null) {
			System.out.println("Unknown stop ("+Peer2+")rel!!");			
		}
		Relationship rel= new Relationship(start,stop,t);
		Rels.add(rel);
		return rel;
	}
	public void addRelationship(String Peer1, String Peer2, String type) {
		//TODO
	}

	public void writeNeo(){
		Session session = driver.session();
		int i=0;
		String statement="CREATE \n";


		//Create all the Peers
		for (Map.Entry<String, Peer> entry : getPeers().entrySet()){
			//System.out.println(entry.getKey() + "/" + entry.getValue());
			if(i!=0) {
				statement+=",\n";
			} else i++;
			Peer p=entry.getValue();
			statement +="("+p.getName()+":"+p.type.toString()+"{name:'"+p.getName()+"'})";
		}

		//Create all Relationships
		for (Relationship rel: Rels ){
			statement+=",\n";
			statement +="("+rel.startNode.getName()+") -[:"+rel.type.toString()+"]-> ("+rel.endNode.getName()+")";
		}
//		System.out.println(statement);
		session.run(statement);
		session.close();
	}

	public void readNeo(String PeerType, String RelType) throws ClassNotFoundException{
		//TODO: Implementation depends on adds with string...
		Session session = driver.session();

		String statement="";
		StatementResult result=null;

		Class PeerTypeClass;
		Peer tmpPeer=null;
		PeerTypeClass = Class.forName(PeerType);
		for (Object t: PeerTypeClass.getEnumConstants()){
			System.out.println(t);
			statement ="MATCH (p:"+t.toString()+") return p.name AS name ";
			result=session.run(statement);
			while ( result.hasNext() )
			{
				Record record = result.next();
				addPeer(record.get("name").asString(), t.toString());
				record.fields();
			}
		};

		Class RelTypeClass;

		RelTypeClass = Class.forName(RelType);
		for (Object t: RelTypeClass.getEnumConstants()){
			System.out.println(t);
			statement ="MATCH (p:"+t.toString()+") return p.name AS name ";
			result=session.run(statement);
			while ( result.hasNext() )
			{
				Record record = result.next();
				addPeer(record.get("name").asString(), t.toString());
			}
			statement ="MATCH (start) -[r:"+t.toString()+"]->(stop) return start.name AS start, stop.name AS stop";
			result=session.run(statement);
			while ( result.hasNext() )
			{
				Record record = result.next();
				addRelationship(record.get("start").asString(),record.get("stop").asString(),t.toString());
			}	

		};

		//		
		//		
		//		//Create Provide relationships
		//		statement ="MATCH (csp) -[r:provides]->(s) return csp.name AS csp, s.name AS service, labels(s) AS servicetype";
		//		result=session.run(statement);
		//		while ( result.hasNext() )
		//    	{
		//    	    Record record = result.next();
		//    	    addProvides(record.get("csp").asString(),record.get("service").asString());
		//    	}	


		//TODO: SLAs and SLATs

		session.close();
	}

	
	public void print() {
		for (Map.Entry<String, Peer> entry : getPeers().entrySet()){
			System.out.println("Peer("+entry.getValue().type+"): " + entry.getValue().getName());
			Properties ps=entry.getValue().getProperties();
			for (Object p: ps.keySet()) {
				System.out.println("----->"+p.toString()+":"+ps.getProperty(p.toString()));
			}
		}

		//Create all Relationships
		for (Relationship rel: Rels ){
			if(rel==null) {
				System.out.println("relationship missing");
				System.exit(-1);
			}
			System.out.print("Rels("+rel.type+"): ");
			System.out.print(rel.getStartNode().getName() + "->");
			System.out.print(rel.getEndNode().getName());
			System.out.println();

		}

	}
	//	public static void main(String args[]) {
	//		graphModel gm= new graphModel();
	//		macmPeerType p=macmPeerType.CSP;
	//		macmRelationshipType r=macmRelationshipType.grants;
	//		gm.readNeo("multicloud.models.macm.macmPeerType","multicloud.models.macm.macmRelationshipType");
	//	}
}
package eu.musaproject.multicloud.models.engines;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import eu.musaproject.multicloud.converters.NIST_db2Neo;
import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class SLAcomposerFile {
	MACM mcapp;
	NeoDriver neo;
	SLAreasoner slar;

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
	public SLAcomposerFile() throws SQLException {
		mcapp=new MACM();
		neo= new NeoDriver();
		try {
			neo.cleanDB();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		NIST_db2Neo.createNISTgraph();
		slar=new SLAreasoner(mcapp);
	}

	public void setAppFromFile(String appModel) throws IOException {
		//		mcapp=new MACM();
		neo.cleanDB();
		neo.executeFromFile(appModel);
		mcapp.readNeo();		
	}

	public void setModelfromNeo(String model) {
		//Set MACM aplication model from a string containing neo
		mcapp=new MACM();
		neo.execute(model);
		mcapp.readNeo();		
	}

	public macmSLA addSLAfromCSV(String slaname, String provider, String csv) {
		csv2neo.createGraphFromCsv(csv,"SLA");
		mcapp.addSLA(slaname);
		Relationship r=mcapp.addGrants(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLA sla=new macmSLA();
		sla.readNeo(slaname);
		slar.addSLAtoKB(sla);
		return sla;
	}

	public macmSLA addSLAfromCSVcontent(String slaname, String provider, String csv) {
		csv2neo.createGraphFromCsvContent(slaname,csv);
		mcapp.addSLA(slaname);
		Relationship r=mcapp.addGrants(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLA sla=new macmSLA();
		sla.readNeo(slaname);
		slar.addSLAtoKB(sla);
		return sla;
	}

	public macmSLA addSLATfromCSV(String slaname, String provider, String csv) {
		//		System.out.println("Adding SLAT for "+provider);
		csv2neo.createGraphFromCsv(csv,"SLAT");
		mcapp.addSLAT(slaname);
		Relationship r=mcapp.addSupports(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLAT slat=new macmSLAT();
		slat.readNeo(slaname);
		slar.addSLATtoKB(slat);
		//		slat.print();
		return slat;
	}

	public macmSLA addSLATfromWSAGcontent(String slaname, String provider, String wsag) {
		wsag2neo.wsagGraph(wsag,"SLAT");
		mcapp.addSLAT(slaname);
		Relationship r=mcapp.addSupports(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLAT slat=new macmSLAT();
		slat.readNeo(slaname);
		slar.addSLATtoKB(slat);
		return slat;
	}

	public macmSLA addSLAfromWSAG(String slaname, String provider, String wsagFileName) throws IOException {
		String wsag=readFile(wsagFileName,StandardCharsets.UTF_8);
		wsag2neo.wsagGraph(wsag,"SLA");
		mcapp.addSLA(slaname);
		Relationship r=mcapp.addGrants(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLA sla=new macmSLA();
		sla.readNeo(slaname);
		slar.addSLAtoKB(sla);
		return sla;
	}
	public macmSLA addSLATfromWSAG(String slaname, String provider, String wsagFileName) throws IOException {
		String wsag=readFile(wsagFileName,StandardCharsets.UTF_8);
		wsag2neo.wsagGraph(wsag,"SLAT");
		mcapp.addSLAT(slaname);
		Relationship r=mcapp.addSupports(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLAT slat=new macmSLAT();
		slat.readNeo(slaname);
		slar.addSLATtoKB(slat);
		return slat;
	}
	public macmSLA addSLATfromCSVcontent(String slaname, String provider, String csv) {
		csv2neo.createGraphFromCsvContent(slaname, csv);
		mcapp.addSLAT(slaname);
		Relationship r=mcapp.addSupports(provider, slaname);
		mcapp.syncNeoRelationship(r);
		macmSLAT slat=new macmSLAT();
		slat.readNeo(slaname);
		slar.addSLATtoKB(slat);
		return slat;
	}

	public void prepare() {
		slar.evaluateCompositionRulesByFamily("AC");
		slar.evaluateCompositionRulesByFamily("PE");
		slar.evaluateCompositionRulesByFamily("IA");
		slar.evaluateCompositionRulesByFamily("SI");
		slar.evaluateCompositionRulesByFamily("RA");
		slar.evaluateCompositionRulesByFamily("SA");
		slar.evaluateCompositionRulesByFamily("SC");
		slar.evaluateCompositionRulesByFamily("IR");
		slar.evaluateCompositionRulesByFamily("MP");
		slar.evaluateCompositionRulesByFamily("CM");
		slar.evaluateCompositionRulesByFamily("CA");
		slar.evaluateCompositionRulesByFamily("AT");
	}

	public macmSLA ComposedSLAinNeo(String provider, String slaname) {
		System.out.println("Querying for "+provider);
		macmSLA sla=slar.getSLA(provider);
		sla.writeNeo();
		mcapp.addSLA(slaname);
		Relationship r=mcapp.addGrants(provider, slaname);
		mcapp.syncNeoRelationship(r);
		return sla;
	}

	public macmSLA getSLAfromNeo(String slaname) {
		macmSLA sla =new macmSLA();
		sla.readNeo(slaname);
		return sla;
	}
	public void compose() {
		Map <String,Peer> map= mcapp.getPeers();
		String tmp;
		System.out.print("Start Composing ");
		Set<String> keys=map.keySet();
		String keystring[]= new String[keys.size()];
		Iterator iter=keys.iterator();
		int i=0;
		while (iter.hasNext()) {
			keystring[i]=new String((String) iter.next());
			i++;
		}
		Peer tmpPeer;
		System.out.println("Composing Node by node..");
		for (i=0;i<keystring.length;i++) {
			tmpPeer=map.get(keystring[i]);
			tmp=tmpPeer.getName();
			System.out.println("Compose for"+tmp);
			if (tmpPeer.type==macmPeerType.IaaS) {
				ComposedSLAinNeo(tmp,tmp+"_sla");
			} else if (tmpPeer.type==macmPeerType.PaaS) {
				ComposedSLAinNeo(tmp,tmp+"_sla");
			} else if (tmpPeer.type==macmPeerType.SaaS) {
				ComposedSLAinNeo(tmp,tmp+"_sla");
			}
		}
	}

	static public void main(String args[]) {
		SLAcomposerFile slac;
		try {
			slac = new SLAcomposerFile();
			Scanner scan=new Scanner(System.in);
			slac.setAppFromFile("src/main/resources/Composition/test/application.cyber.txt");
			slac.addSLAfromWSAG("Amazon_sla","Amazon", "src/main/resources/Composition/test/Amazon_sla.xml");
			slac.addSLATfromWSAG("vm_slat", "vm", "src/main/resources/Composition/test/vm_slat.xml");
			slac.addSLATfromWSAG("db_slat", "db", "src/main/resources/Composition/test/db_slat.xml");
			slac.addSLATfromWSAG("webapp_slat", "webapp", "src/main/resources/Composition/test/webapp_slat.xml");
			slac.prepare();
			String query="";

			//Prolog Console
			//			System.out.println("Go test");
			//			while (!query.equals("stop")) {
			//				System.out.println("Query");System.out.flush();
//							query=scan.nextLine();				
			//				slac.slar.query(query);
			//			} 
			System.out.println("Press Enter to Compose");
			query=scan.nextLine();				
			slac.compose();
			System.out.println("Press Enter to retrieve sla");
			query=scan.nextLine();				

			//			slac.compose("vm", "vm_sla");
			//			slac.compose("db", "db_sla");
			//			slac.compose("webapp", "webapp_sla");
			macmSLA wsla=slac.getSLAfromNeo("webapp_sla");
			wsla.print();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

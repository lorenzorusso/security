package eu.musaproject.multicloud.models.engines;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import com.ugos.jiprolog.engine.JIPEngine;
import com.ugos.jiprolog.engine.JIPQuery;
import com.ugos.jiprolog.engine.JIPTerm;
import com.ugos.jiprolog.engine.JIPVariable;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.SLAPeerType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;

public class SLAreasoner {
	MACM mcapp;
	JIPEngine jip;
	NISTrules nist;

	public SLAreasoner() throws SQLException {
		jip = new JIPEngine();
		nist=new NISTrules();
		//		nist.initAC();
		nist.createNISTKB(jip);
	}

	public SLAreasoner(MACM m) throws SQLException {
		jip = new JIPEngine();
		nist=new NISTrules();
		nist.createNISTKB(jip);
		//		nist.initAC();
		//		nist.ACrules();
		nist.rules("AC");
		nist.rules("PE");
		nist.rules("IA");
		nist.rules("SI");
		nist.rules("RA");
		nist.rules("SA");
		nist.rules("SC");
		nist.rules("IR");
		nist.rules("MP");
		nist.rules("CM");
		nist.rules("CA");
		nist.rules("AT");	
		mcapp=m;
	}

	public void setMACM(MACM m) {
		mcapp=m;
	}
	public void addSLAtoKB(macmSLA sla) {
		String slaname=sla.getName();
		String granterName=slaname.toLowerCase().substring(0,slaname.length()-4);
		Map<String,Peer> peers=sla.getPeers();

		JIPTerm queryTerm = null;
		for(Map.Entry<String, Peer> entry : peers.entrySet()) {
			if(entry.getValue().type==SLAPeerType.SecurityControl) {
				queryTerm = jip.getTermParser().parseTerm("sla("+entry.getValue().getName().toLowerCase()+","+granterName+").");
				jip.asserta(queryTerm);
//				System.out.println(queryTerm);

			}
		}		
	}

	public void addSLATtoKB(macmSLA sla) {
		String slaname=sla.getName();
		String granterName=slaname.toLowerCase().substring(0,slaname.length()-5);
		Map<String,Peer> peers=sla.getPeers();
		ArrayList<String> rules =new ArrayList<String>();
		JIPTerm queryTerm = null;
		for(Map.Entry<String, Peer> entry : peers.entrySet()) {
			if(entry.getValue().type==SLAPeerType.SecurityControl) {
				queryTerm = jip.getTermParser().parseTerm("slat("+entry.getValue().getName().toLowerCase()+","+granterName+").");
				jip.asserta(queryTerm);
				//				System.out.println(queryTerm);

			}
		}		
	}

	public void addCompositionRule(String term){
		JIPTerm queryTerm = null;
		//		System.out.println("Add rule:"+term);
		if (term!=null) {
			queryTerm = jip.getTermParser().parseTerm(term);
			//			System.out.println("composition rules");
			jip.asserta(queryTerm);
		}
	}


	public void evaluateCompositionRulesByFamily(String controlfamily) {
		//TODO
		//Evaluate Control Family
		//		System.out.println("Composing for AC");
		String CF=StringUtils.lowerCase(controlfamily);
		String term="";
		Map<String,Peer> peers=mcapp.getPeers();
		List<Relationship> rels=mcapp.getRelationship();

		//A Map that for each node lists the pair relationship_role in which it is involved
		Map<Peer,List<Relationship>> noderels=new  HashMap<Peer,List<Relationship>>();

		for(Map.Entry<String,Peer> entry : peers.entrySet()) {
			List<Relationship> relslist=new ArrayList<Relationship>();
			noderels.put(entry.getValue(), relslist);
		}

		Relationship tmprel;
		for (int i=0;i<rels.size();i++) {
			tmprel=rels.get(i);
			//			System.out.println(tmprel.startNode.getName()+" "+tmprel.endNode.getName());
			List <Relationship> ls=noderels.get(tmprel.startNode);
			if (ls==null) {
				ls=new ArrayList<Relationship>();
			}
			ls.add(tmprel);
			List <Relationship> le=noderels.get(tmprel.endNode);
			if (le==null) {
				le=new ArrayList<Relationship>();
			}
			le.add(tmprel);
		}

		for(Map.Entry<Peer,List<Relationship>> entry : noderels.entrySet()) {
			String rule=nist.Compose(controlfamily, entry.getKey(), entry.getValue());
			//			System.out.println(rule);
			//			String rule=nist.CompositionRule(control, entry.getKey(), entry.getValue());
			//			String[] Terms=nist.String2prolog("sla(X,"+entry.getKey().getName()+")", rule);
			String[] Terms=nist.String2prologByFamily("sla(X,"+entry.getKey().getName()+")","controlfamily(X,"+CF+")", rule);
			for (int i=0;i<Terms.length;i++) {
				System.out.println(Terms[i]);
				//				addCompositionRule("controlfamily(X,"+CF+")");
				addCompositionRule(Terms[i]);			
			}
		}
	}

	public void evaluateCompositionRules(String control) {
		//TODO
		//Evaluate Control Family
		String CF=StringUtils.substringBefore(control, "_").toLowerCase();
		String SC=control.toLowerCase();
		String term="";
		Map<String,Peer> peers=mcapp.getPeers();
		List<Relationship> rels=mcapp.getRelationship();

		//A Map that for each node lists the pair relationship_role in which it is involved
		Map<Peer,List<Relationship>> noderels=new  HashMap<Peer,List<Relationship>>();

		for(Map.Entry<String,Peer> entry : peers.entrySet()) {
			List<Relationship> relslist=new ArrayList<Relationship>();
			noderels.put(entry.getValue(), relslist);
		}

		Relationship tmprel;
		for (int i=0;i<rels.size();i++) {
			tmprel=rels.get(i);
			noderels.get(tmprel.startNode).add(tmprel);
			noderels.get(tmprel.endNode).add(tmprel);
		}

		for(Map.Entry<Peer,List<Relationship>> entry : noderels.entrySet()) {
			String rule=nist.Compose(control, entry.getKey(), entry.getValue());
			//			System.out.println(rule);
			//			String rule=nist.CompositionRule(control, entry.getKey(), entry.getValue());
			String[] Terms=nist.String2prolog("sla(X,"+entry.getKey().getName()+")", rule);
			for (int i=0;i<Terms.length;i++) {
				addCompositionRule(Terms[i]);			
			}
		}
	}

	public void DB() {
		jip.compileFile("target/prolog.pl");
	}

	public void query(String query) {
		JIPTerm queryTerm=jip.getTermParser().parseTerm(query);

		// open Query
		JIPQuery jipQuery = jip.openSynchronousQuery(queryTerm);
		JIPTerm solution;

		// Loop while there is another solution
		while (jipQuery.hasMoreChoicePoints())
		{
			solution = jipQuery.nextSolution();
			if (solution!=null) {
				//				System.out.println(solution);
				JIPVariable[] vars = solution.getVariables();
				for (JIPVariable var : vars) {
					if (!var.isAnonymous()) {
						System.out.println(var.toString(jip));
					}
				}
			} else {
				System.out.println("no solution");
			}
		}
	}

	public boolean checkControlinSLA(String control,String provider) {
		String query="sla("+control+","+provider+").";
		System.out.println("checkControlinSLA "+query);
		boolean result=false;
		JIPTerm queryTerm=jip.getTermParser().parseTerm(query);

		// open Query
		JIPQuery jipQuery = jip.openSynchronousQuery(queryTerm);
		JIPTerm solution;

		solution = jipQuery.nextSolution();
		if (solution!=null) {
						System.out.println("Control "+control+" in "+provider+" sla");
			result=true;
		}		else {
						System.out.println("Control "+control+" NOT in "+provider+" sla");
			result=false;
		}
		return result;
	}

	public macmSLA getSLA2(String provider) {
		System.out.println("getSLA2");
		String slaname=provider+"_sla";
		macmSLA sla=new macmSLA(slaname);
		//		String[] controls= new String[]{"ac_1", "ac_2", "ac_3"};
		List<String> controls=nist.getControlsList();
		System.out.println("controls "+controls.size());

		for(int i=0;i<controls.size();i++) {
			if (checkControlinSLA(controls.get(i),provider)) {
				sla.addSecurityControl(controls.get(i));
			}
		}
		return sla;
	}

	public macmSLA getSLA(String name) {
		System.out.println("getSLA");
		macmSLA sla=new macmSLA(name+"_sla");
		String query="sla(X,"+name+").";
		JIPTerm queryTerm=jip.getTermParser().parseTerm(query);
		System.out.println("queryTerm");

		// open Query
		JIPQuery jipQuery = jip.openSynchronousQuery(queryTerm);
		JIPTerm solution;

		// Loop while there is another solution
		while (jipQuery.hasMoreChoicePoints())
		{
			solution = jipQuery.nextSolution();
			if (solution!=null) {
				System.out.println(solution);

				JIPVariable[] vars = solution.getVariables();
				for (JIPVariable var : vars) {
					if (!var.isAnonymous()) {
						sla.addSecurityControl(var.toString(jip));
						System.out.println(var.toString(jip));
					}
				}
			} else {
				System.out.println("no solution");
			}
			System.out.println("Press Enter to Get Next solution, if exist");
			Scanner scan=new Scanner(System.in);
			scan.nextLine();	
		}
		return sla;
	}




	static public void main(String args[]){
		MACM mcapp=new MACM();
		mcapp.readNeo();
		mcapp.print();
		SLAreasoner slar;
		try {
			slar = new SLAreasoner(mcapp);
			macmSLA cspsla=new macmSLA();
			macmSLA vmsla=new macmSLA();
			cspsla.readNeo("Amazon_sla");
			vmsla.readNeo("vm_sla");
			slar.addSLAtoKB(cspsla);
			slar.addSLATtoKB(vmsla);	
			slar.evaluateCompositionRules("AC");
			slar.query("sla(X,csp_sla).");
			//		slar.addCompositionRule("sla(X,vm):-sla(X,csp_sla),slat(X,vmubuntu_sla)");
			macmSLA csla=slar.getSLA("webapp_sla");
			csla.print();
			csla.writeNeo();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

package eu.musaproject.multicloud.models.engines;

import java.io.Console;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Scanner;

import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class SLAComposerWSAG extends SLAcomposer {
	
	public SLAComposerWSAG() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}
	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	@Override
	public void setMACMinNeo() {
		// TODO Auto-generated method stub
		try {
			neo.executeFromFile("src/main/resources/Composition/test/application.cyber.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void setSLATinNeo(String provider) {
		// TODO Auto-generated method stub
		String filename="src/main/resources/Composition/test/"+provider+"_slat.xml";
		String wsag;
		try {
			wsag = readFile(filename,StandardCharsets.UTF_8);
			wsag2neo.wsagGraph(wsag,"SLAT");
			mcapp.addSLAT(provider+"_slat");
			Relationship r=mcapp.addSupports(provider, provider+"_slat");
			mcapp.syncNeoRelationship(r);
			macmSLAT slat=new macmSLAT();
			slat.readNeo(provider+"_slat");
			slar.addSLATtoKB(slat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void setSLAinNeo(String provider) {
		String filename="src/main/resources/Composition/test/"+provider+"_sla.xml";
		String wsag;
		try {
			wsag = readFile(filename,StandardCharsets.UTF_8);
			wsag2neo.wsagGraph(wsag,"SLA");
			mcapp.addSLA(provider+"_sla");
			Relationship r=mcapp.addGrants(provider, provider+"_sla");
			mcapp.syncNeoRelationship(r);
			macmSLA sla=new macmSLA();
			sla.readNeo(provider+"_sla");
			slar.addSLAtoKB(sla);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public macmSLA getSLAfromNeo(String provider) {
		// TODO Auto-generated method stub
		macmSLA sla =new macmSLA();
		sla.readNeo(provider+"_sla");
//		sla.print();
		return sla;
	}

	@Override
	public macmSLAT getSLATfromNeo(String provider) {
		// TODO Auto-generated method stub
		macmSLAT sla =new macmSLAT();
		sla.readNeo(provider+"_slat");
//		sla.print();
		return sla;
	}
	
	public void query(String query) {
		slar.query(query);
	}
	
	public static void main(String args[]){
		try {
			SLAComposerWSAG comp=new SLAComposerWSAG();
			comp.session();
//			String query="";
//			System.out.println("Prolog query:");
//			while(!query.equalsIgnoreCase("exit")) {
//				Scanner scan = new Scanner(System.in);
//				query = scan.nextLine();
//				System.out.println("Prolog query:"+query);
//				comp.query(query);
//			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

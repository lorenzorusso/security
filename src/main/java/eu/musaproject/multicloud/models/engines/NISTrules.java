package eu.musaproject.multicloud.models.engines;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.stringtemplate.v4.ST;

import com.ugos.jiprolog.engine.JIPEngine;
import com.ugos.jiprolog.engine.JIPTerm;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.utilities.NeoDriver;
import eu.specs.datamodel.control_frameworks.nist.NISTSecurityControl;
import eu.specs.datamodel.sla.sdt.WeightType;

public class NISTrules {
	//	Map<String,Map<macmRelationshipType,Map<String,String[]>>> RuleTable;
	static Map<String,ST> RuleTable;
	String cacca;
	Map<String,NISTSecurityControl> ControlsMap; 
	List<String> controls;

	
	public NISTrules() {
		RuleTable= new HashMap<String,ST>();
		controls=null;
		ControlsMap=new HashMap<String,NISTSecurityControl>() ;
	}

	public List<String> getControlsList() {
		return controls;
	}
	
	public Map<String,NISTSecurityControl> getControlsMap() {
		return ControlsMap;
	}
	
	public List<String> UpdateControlsList() throws SQLException {
		controls=new ArrayList<String>();
		Connection conn = null;
		Statement stmt = null;
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/slagenerator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "root");
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select control_id,control_name,family_id,enhancement,control_description,control from controls ");
		while (rs.next()) {
			NISTSecurityControl control=new NISTSecurityControl();
			String SC=(String) rs.getObject(1);
			SC=SC.replace('-', '_');
			SC=SC.replace('(', '_');
			SC=SC.replace(')', ' ');
			
			control.setId((String) rs.getObject(1));
			control.setName((String) rs.getObject(2));
			control.setControlFamily((String) rs.getObject(3));
			control.setControlEnhancement(((Integer) rs.getObject(4)).toString());
			control.setControlDescription((String) rs.getObject(5));
			control.setSecurityControl(((Integer) rs.getObject(6)).toString());
			control.setImportanceWeight(WeightType.MEDIUM);

			ControlsMap.put(SC.toLowerCase(), control);
			controls.add(SC.toLowerCase());
//			System.out.println(SC.toLowerCase());
		}
		return controls;
	}
	public void createNISTKB(JIPEngine jip) throws SQLException {
		controls=new ArrayList<String>();
		String NeoPayLoad;
		String prolog="";
		Connection conn = null;
		Statement stmt = null;
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/slagenerator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "root");
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select control_id,family_id from controls ");
		
		System.out.println("NIST KB");

		boolean start=true;
		while (rs.next()) {
			String SC=(String) rs.getObject(1);
			String CF=(String) rs.getObject(2);
			SC=SC.replace('-', '_');
			SC=SC.replace('(', '_');
			SC=SC.replace(')', ' ');
			controls.add(SC.toLowerCase());
			if (start) {
				start=false;
			} 
			prolog="controlfamily("+SC+","+ CF+")";
//			System.out.println(prolog);
			JIPTerm queryTerm = jip.getTermParser().parseTerm(prolog.toLowerCase());
			jip.asserta(queryTerm);
		}

	}


	static public void init(){
		ST rule= new ST("sla(X,<n>:-sla(X,<s>),slat(X,<n>) <rule>.");
		if (RuleTable==null) {
			System.out.println("cacca");
		}
		RuleTable.put("AC_provides_target",rule);
		rule= new ST("sla(X,<n>:-slat(X,<t>).");
		RuleTable.put("AC_hosts_source",rule);

	}
	public static String CompositionRule(String Control, Peer p, List<Relationship> l){
		String rule="";
		String relrulename;
		ST tmp;
		String[] rules=new String[l.size()];
		Relationship tmpRel;
		//provides

		for(int i=0;i<l.size();i++) {
			tmpRel=l.get(i);
			relrulename=Control+"_"+tmpRel.getType();
			if(p==tmpRel.startNode) relrulename+="_source";
			else relrulename+="_target";
			tmp=RuleTable.get(relrulename);
			if (tmp!=null) {
				tmp.add("S", tmpRel.startNode.getName());
				tmp.add("T", tmpRel.endNode.getName());
				rules[i]=tmp.render();
				tmp.remove("S");
				tmp.remove("T");		
			}
		}

		tmp=RuleTable.get(Control);
		String composed=RuleTable.get(Control+"init").render();
		for (int i=0; i<rules.length; i++) {
			tmp.add("composed", composed);
			tmp.add("rule", rules[i]);
			//			System.out.println("compose: "+composed+" with "+rules[i]);
			composed=tmp.render();
			tmp.remove("composed");
			tmp.remove("rule");
		}	

		return composed;
	}

	public static String Compose(String Control, Peer p, List<Relationship> l) {
		String composed="";
		String relrulename;
		ST tmp;
		String[] rules=new String[l.size()];
		Relationship tmpRel;
		for(int i=0;i<l.size();i++) {
			tmpRel=l.get(i);
			relrulename=Control+"_"+tmpRel.getType();
			//			System.out.println(relrulename+p.getName());
			if (p==tmpRel.startNode) relrulename+="_source";
			else if (p==tmpRel.endNode) relrulename+="_target";
			else relrulename=null;
			tmp=RuleTable.get(relrulename);
//			System.out.println(relrulename);
			if ((tmp!=null)&&(relrulename!=null)) {
				tmp.add("S", tmpRel.startNode.getName());
				tmp.add("T", tmpRel.endNode.getName());
				tmp.add("X",composed);
				composed=tmp.render();
				tmp.remove("S");
				tmp.remove("T");	
				tmp.remove("X");
			}
		}

		tmp=RuleTable.get(Control+"start");
		tmp.add("X", composed);
		tmp.add("S", p.getName());
		composed=tmp.render();
		tmp.remove("X");
		tmp.remove("S");
		return composed;
	}


	static public void initAC() {
		ST rule;
		rule= new ST("<composed> AND <rule>");
		RuleTable.put("AC",rule);
		rule= new ST("1");
		RuleTable.put("ACinit",rule);

		rule= new ST("1");
		RuleTable.put("AC_provides_source",rule);
		rule= new ST("sla(X,<S>) AND slat(X,<T>)");
		RuleTable.put("AC_provides_target",rule);
		rule= new ST("1 AND slat(X,<T>)");
		RuleTable.put("AC_hosts_source",rule);
		rule= new ST("sla(X,<S>)");
		RuleTable.put("AC_hosts_target",rule);
		rule= new ST("sla(X,<T>)");
		RuleTable.put("AC_uses_source",rule);
		rule= new ST("1");
		RuleTable.put("AC_uses_target",rule);
	}

	public static String CompositionRule(){
		// Nodo vm: csp-[:provides]->vm-[:hosts]->W ; vm-[:hosts]->DB;
		ST tmp;
		int rels=3;
		String[] rules=new String[rels];

		//provides
		tmp=RuleTable.get("AC_provides_target");
		tmp.add("S", "csp");
		tmp.add("T", "vm");
		rules[0]=tmp.render();
		tmp.remove("S");
		tmp.remove("T");


		//hosts source
		tmp=RuleTable.get("AC_hosts_source");
		tmp.add("S", "vm");
		tmp.add("T", "W");
		rules[1]=tmp.render();
		tmp.remove("S");
		tmp.remove("T");

		tmp=RuleTable.get("AC_hosts_source");
		tmp.add("S", "vm");
		tmp.add("T", "DB");
		rules[2]=tmp.render();
		tmp.remove("S");
		tmp.remove("T");

		//hosts target
		//miss

		//hosts source
		//miss

		//hosts target
		//miss

		//compose
		tmp=RuleTable.get("AC");
		String composed=RuleTable.get("ACinit").render();
		for (int i=0; i<rels; i++) {
			tmp.add("composed", composed);
			tmp.add("rule", rules[i]);
			//			System.out.println("compose: "+composed+" with "+rules[i]);
			composed=tmp.render();
			tmp.remove("composed");
			tmp.remove("rule");
		}
		return composed; 

	}

	public static String[] String2prolog(String goal, String S) {
		String[] Terms=S.split("OR");
		for (int i=0;i<Terms.length;i++){
			//			System.out.println("Build term for:"+Terms[i]);
			String ands=Terms[i];
			Terms[i]="";
			String[] clauses=ands.split("AND");
			for(int j=0; j<clauses.length;j++) {
				if((!clauses[j].contains("1"))&&!clauses[j].equals(" ")){
					Terms[i]+=clauses[j];
					Terms[i]+=",";
					//System.out.println("len:"+clauses[j].length());
				}
				//				System.out.print("term for:"+clauses[j]+":"+Terms[i]+"!");
				//				if (Terms[i]!=null) System.out.println(" not null");
			}
			if(Terms[i].equals("")) {
				Terms[i]=null;
			} else {
				Terms[i]=goal+":-"+Terms[i].substring(0, Terms[i].length()-1)+".";
			}
		}
		return Terms;		
	}

	public static String[] String2prologByFamily(String goal,String familyrule, String S) {
		String[] Terms=S.split("OR");
		for (int i=0;i<Terms.length;i++){
			//			System.out.println("Build term for:"+Terms[i]);
			String ands=Terms[i];
			Terms[i]="";
			String[] clauses=ands.split("AND");
			for(int j=0; j<clauses.length;j++) {
				if((!clauses[j].contains("1"))&&!clauses[j].equals(" ")){
					Terms[i]+=clauses[j];
					Terms[i]+=",";
					//System.out.println("len:"+clauses[j].length());
				}
				//				System.out.print("term for:"+clauses[j]+":"+Terms[i]+"!");
				//				if (Terms[i]!=null) System.out.println(" not null");
			}
			if(Terms[i].equals("")) {
				Terms[i]=null;
			} else {
				Terms[i]=goal+":-"+familyrule+","+Terms[i].substring(0, Terms[i].length()-1)+".";
			}
		}
		return Terms;		
	}

	static public void ACrules(){
		ST rule;
		rule= new ST("slat(X,<S>) AND <X>");
		RuleTable.put("ACstart",rule);
		//		rule= new ST("");
		//		RuleTable.put("ACinit",rule);

		rule= new ST("1");
		RuleTable.put("AC_provides_source",rule);
		rule= new ST("sla(X,<S>) AND <X>");
		RuleTable.put("AC_provides_target",rule);
		rule= new ST("<X>");
		RuleTable.put("AC_hosts_source",rule);
		rule= new ST("sla(X,<S>) AND <X>");
		RuleTable.put("AC_hosts_target",rule);
		rule= new ST("sla(X,<T>) AND <X>");
		RuleTable.put("AC_uses_source",rule);
		rule= new ST("<X>");
		RuleTable.put("AC_uses_target",rule);
	}


	static public void rules(String CF) {
		Properties prop = new Properties();
		InputStream input = null;
		ST rule;

		try {

			input = new FileInputStream("src/main/resources/Composition/NIST/NIST.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println(CF+"start:"+prop.getProperty(CF+"start"));
			rule= new ST(prop.getProperty(CF+"start"));
			RuleTable.put(CF+"start",rule);
			//		rule= new ST("");
			//		RuleTable.put("ACinit",rule);

			rule= new ST(prop.getProperty(CF+"_provides_source"));
			RuleTable.put(CF+"_provides_source",rule);
			rule= new ST(prop.getProperty(CF+"_provides_target"));
			RuleTable.put(CF+"_provides_target",rule);
			rule= new ST(prop.getProperty(CF+"_hosts_source"));
			RuleTable.put(CF+"_hosts_source",rule);
			rule= new ST(prop.getProperty(CF+"_hosts_target"));
			RuleTable.put(CF+"_hosts_target",rule);
			rule= new ST(prop.getProperty(CF+"_uses_source"));
			RuleTable.put(CF+"_uses_source",rule);
			rule= new ST(prop.getProperty(CF+"_uses_target"));
			RuleTable.put(CF+"_uses_target",rule);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	static public void tablePrint(List <macmSLA> slas) throws SQLException, FileNotFoundException {
		int n=slas.size();
		int i;
		PrintWriter out = new PrintWriter("target/table.csv");
		out.print("SC");
		for(i=0;i<n;i++) {
			out.print(", ");
			out.print(slas.get(i).getName());
		}
		out.println("");

		Connection conn = null;
		Statement stmt = null;
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/slagenerator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "root");
		stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select control_id,family_id from controls ");
		
		boolean start=true;
		while (rs.next()) {
			String SC=(String) rs.getObject(1);
			String CF=(String) rs.getObject(2);
			SC=SC.replace('-', '_');
			SC=SC.replace('(', '_');
			SC=SC.replace(')', ' ');
			if (start) {
				start=false;
			}
			out.print(SC);
			for(i=0;i<n;i++) {
				out.print(", ");
				if (slas.get(i).hasControl(SC))
					out.print("1");
				else 
					out.print("0");
			}
			out.println("");
		}
	}
	
	
	
	static public void main(String args[]) throws SQLException {
		NISTrules r= new NISTrules();
		r.UpdateControlsList();
//		r.rules("AC");
//		//		r.initAC();
//		r.ACrules();
//
//		MACM macm= new MACM();
//		try {
//			NeoDriver.cleanDB();
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		try {
//			NeoDriver.executeFromFile("src/main/resources/CS3M/webapp.cyber");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
////		System.out.println("start");
//		macm.readNeo();
//		String rule;
//		rule=r.Compose("AC",macm.getPeers().get("db"), macm.getRelationship());
////		System.out.println(rule);
////		System.out.println(String2prolog("sla(X,vm)",rule)[0]);
//
//		//		String rule=CompositionRule();
//		//		String rule="sla(X,d) OR sla(X,vm) AND sla(Y,vm)";
//		//
//		//		String[] prolog=String2prolog("sla(X,vm)",rule);
//		//
//		//		for(int i=0; i<prolog.length;i++) {
//		//			System.out.println(prolog[i]);
//		//		}
//
	}
}

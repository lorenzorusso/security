package eu.musaproject.multicloud.models.engines;

import java.io.Console;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

import eu.musaproject.multicloud.converters.csv2neo;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class CS3McomposerConsole extends SLAcomposer {
	
	public CS3McomposerConsole() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setMACMinNeo() {
		// TODO Auto-generated method stub
		try {
			neo.executeFromFile("src/main/resources/CS3M/webapp.cyber");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void setSLATinNeo(String provider) {
		// TODO Auto-generated method stub
		csv2neo.createGraphFromCsv("src/main/resources/CS3M/"+provider+"_slat","SLAT");
		mcapp.addSLAT(provider+"_slat");
		Relationship r=mcapp.addSupports(provider, provider+"_slat");
		mcapp.syncNeoRelationship(r);
		macmSLAT slat=new macmSLAT();
		slat.readNeo(provider+"_slat");
		slar.addSLATtoKB(slat);
	}

	@Override
	public void setSLAinNeo(String provider) {
		// TODO Auto-generated method stub
		csv2neo.createGraphFromCsv("src/main/resources/CS3M/"+provider+"_sla","SLA");
		mcapp.addSLA(provider+"_sla");
		Relationship r=mcapp.addGrants(provider, provider+"_sla");
		mcapp.syncNeoRelationship(r);
		macmSLA sla=new macmSLA();
		sla.readNeo(provider+"_sla");
		slar.addSLAtoKB(sla);

	}

	@Override
	public macmSLA getSLAfromNeo(String provider) {
		// TODO Auto-generated method stub
		macmSLA sla =new macmSLA();
		sla.readNeo(provider+"_sla");
//		sla.print();
		return sla;
	}

	@Override
	public macmSLAT getSLATfromNeo(String provider) {
		// TODO Auto-generated method stub
		macmSLAT sla =new macmSLAT();
		sla.readNeo(provider+"_slat");
//		sla.print();
		return sla;
	}
	
	public void query(String query) {
		slar.query(query);
	}
	public static void main(String args[]){
		try {
			CS3McomposerConsole comp=new CS3McomposerConsole();
			comp.session();
			String query="";
			System.out.println("Prolog query:");
			while(!query.equalsIgnoreCase("exit")) {
				Scanner scan = new Scanner(System.in);
				query = scan.nextLine();
				comp.query(query);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
